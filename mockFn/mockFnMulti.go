package mockFn

type MockFnMulti interface {
	IgnoringArgs() MockFnMultiCallArgs
	Expecting(expectedArgs ...interface{}) MockFnMultiCall
	AsStub(stubResults ...interface{}) MockFnMultiStub
}

// -----

type mockFnMulti struct {
	mockFn *mockFn
}

func (m *mockFnMulti) IgnoringArgs() MockFnMultiCallArgs {
	newMockCall := &mockedCall{m.mockFn.factory(m.mockFn.suite, m.mockFn.mockSkipFrame+1)}
	newMockCall.IgnoringArgs()
	return &mockFnMultiCallArgs{
		&mockFnMultiCall{&mockFnCall{m.mockFn, newMockCall}}}
}
func (m *mockFnMulti) Expecting(expectedArgs ...interface{}) MockFnMultiCall {
	newMockCall := &mockedCall{m.mockFn.factory(m.mockFn.suite, m.mockFn.mockSkipFrame+1)}
	newMockCall.Expecting(expectedArgs...)
	return &mockFnMultiCall{&mockFnCall{m.mockFn, newMockCall}}
}
func (m *mockFnMulti) AsStub(stubResults ...interface{}) MockFnMultiStub {
	newMockCall := &mockedCall{m.mockFn.factory(m.mockFn.suite, m.mockFn.mockSkipFrame+1)}
	newMockCall.AsStub(stubResults...)
	return &mockFnMultiStub{
		&mockFnMultiCall{&mockFnCall{m.mockFn, newMockCall}}}
}
