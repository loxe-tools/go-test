package mockFn

type MockedFn interface {
	Call(receivedArgs ...interface{}) []interface{}
}
