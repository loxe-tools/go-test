package main

import (
	"bytes"
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/util"
	"gitlab.com/loxe-tools/go-test/internal/generate/input"
	"os"
	"text/template"
)

func cliUsage(defaultValues input.Arguments) func() {
	tmpl, e := template.New("").Parse(cliUsageHelpTemplate)
	if e != nil {
		panic(e)
	}

	type tmplVars struct {
		SourceFile        string
		SourceType        string
		SourceTypeDefault string

		Debug                     string
		DebugDefault              string
		SupportsAnsi              string
		SupportsAnsiDefault       string
		OnlyAsciiTypenames        string
		OnlyAsciiTypenamesDefault string
	}
	vars := tmplVars{
		SourceFile: util.CyanString("--" + input.MetaᐸArgumentsᐳ.SourceFile),
		SourceType: util.CyanString("--" + input.MetaᐸArgumentsᐳ.SourceType),

		Debug:                     util.CyanString("--" + input.MetaᐸArgumentsᐳ.Debug),
		DebugDefault:              util.YellowString(fmt.Sprintf("Default: %t", defaultValues.Debug)),
		SupportsAnsi:              util.CyanString("--" + input.MetaᐸArgumentsᐳ.SupportsANSI),
		SupportsAnsiDefault:       util.YellowString(fmt.Sprintf("Default: %t", defaultValues.SupportsANSI)),
		OnlyAsciiTypenames:        util.CyanString("--" + input.MetaᐸArgumentsᐳ.OnlyAsciiTypenames),
		OnlyAsciiTypenamesDefault: util.YellowString(fmt.Sprintf("Default: %t", defaultValues.OnlyAsciiTypenames)),
	}

	var msg bytes.Buffer
	e = tmpl.Execute(&msg, vars)
	if e != nil {
		panic(e)
	}

	return func() {
		fmt.Println(msg.String())
		os.Exit(0)
	}
}

const cliUsageHelpTemplate = `This CLI is used to generate test mocks, for the Löxe Test library, based on GO type interfaces
	Source-related flags:
		{{.SourceFile}}: The path to the file that contains the base GO interface types
			This flag is always required
		{{.SourceType}}: The identifier of a single GO type (an interface type)
			This flag is optional
			If set, the generator will ignore all types whose name doesn't match this flag value

	Other flags:
		{{.Debug}}: This flag controls the level of runtime log information
			If true, additional runtime information will be displayed (aka DEBUG logs)
			{{.DebugDefault}}
		{{.SupportsAnsi}}: If true, the log output will contain ANSI characters
			Set it to true if you terminal supports ANSI characters
			Currently, ANSI escape codes are used to colorize strings and draw "nested logs"
			Some control sequences can only access visible terminal content. Logs with many "nested child logs" will behave unexpectedly
			If you need to debug something, it can help you
			** EXPERIMENTAL FEATURE **
			{{.SupportsAnsiDefault}}
		{{.OnlyAsciiTypenames}}: If true, the generated typeNames and fileNames will contain only ASCII characters
			If this flag is omitted, the typeNames and fileNames will contain random unicode characters to improve readability
			Note that in some systems this flag needs to be true, due to lack of full unicode support
			{{.OnlyAsciiTypenamesDefault}}
`
