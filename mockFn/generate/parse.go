package main

import (
	"flag"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"gitlab.com/loxe-tools/go-test/internal/generate/input"
)

func parse(defaultValues input.Arguments) input.Arguments {
	help := "use --help instead"
	flag.StringVar(&defaultValues.SourceFile, input.MetaᐸArgumentsᐳ.SourceFile, defaultValues.SourceFile, help)
	flag.StringVar(&defaultValues.SourceType, input.MetaᐸArgumentsᐳ.SourceType, defaultValues.SourceType, help)

	flag.BoolVar(&defaultValues.Debug, input.MetaᐸArgumentsᐳ.Debug, defaultValues.Debug, help)
	flag.BoolVar(&defaultValues.SupportsANSI, input.MetaᐸArgumentsᐳ.SupportsANSI, defaultValues.SupportsANSI, help)
	flag.BoolVar(&defaultValues.OnlyAsciiTypenames, input.MetaᐸArgumentsᐳ.OnlyAsciiTypenames, defaultValues.OnlyAsciiTypenames, help)

	flag.Usage = cliUsage(defaultValues)
	flag.Parse()

	log := logCLI.NewLogCLI(defaultValues.Debug, defaultValues.SupportsANSI)
	input.PrintArguments(defaultValues, log)

	return defaultValues
}
