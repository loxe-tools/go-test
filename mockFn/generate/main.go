package main

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/brand"
	"gitlab.com/loxe-tools/go-test/internal"
	"gitlab.com/loxe-tools/go-test/internal/generate"
	"gitlab.com/loxe-tools/go-test/internal/generate/input"
)

const cliTitle = "Löxe Test - Mocks generator"

func main() {
	brand.ToStdout(fmt.Sprintf("%s %s", cliTitle, internal.LibraryModuleVersion))

	generate.Generate(parse(defaultArguments))
}

var defaultArguments = input.Arguments{
	"",
	"",
	false,
	false,
	false,
}
