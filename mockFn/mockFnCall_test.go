package mockFn

import (
	"gitlab.com/loxe-tools/go-test/internal"
	"gitlab.com/loxe-tools/go-test/internal/mockCall"
	"reflect"
	"testing"
	"time"
)

func TestMockFnCall(t *testing.T) {
	suite := &internal.TestSuite{t}

	suite.Run("Returning", func(suite *internal.TestSuite) {
		suite.Run("Should call the MockCall 'Returning' method", func(suite *internal.TestSuite) {
			returningCalls := 0
			fnCall := &mockFnCall{mockedCall: &mockedCall{&mockMockCall{
				_Returning: func(functionResults ...interface{}) {
					returningCalls += 1
				},
			}}}
			fnCall.Returning()

			if returningCalls != 1 {
				suite.Fatalf("The 'Returning' method didn't correctly called the MockCall 'Returning' method")
			}
		})
		suite.Run("Should return itself", func(suite *internal.TestSuite) {
			fnCall := &mockFnCall{mockedCall: &mockedCall{&mockMockCall{
				_Returning: func(functionResults ...interface{}) {},
			}}}

			if fnCall.Returning().(*mockFnCall) != fnCall {
				suite.Fatalf("The 'Returning' method returned a wrong value")
			}
		})
		suite.Run("Should forward the given function results to the MockCall 'Returning' method", func(suite *internal.TestSuite) {
			results := []interface{}{1, "value"}
			fnCall := &mockFnCall{mockedCall: &mockedCall{&mockMockCall{
				_Returning: func(functionResults ...interface{}) {
					if !reflect.DeepEqual(functionResults, results) {
						suite.Fatalf("The 'Returning' method is not forwarding the correct arguments to the created call")
					}
				},
			}}}
			fnCall.Returning(results...)
		})
	})
	suite.Run("UsingComparators", func(suite *internal.TestSuite) {
		suite.Run("Should call the MockCall 'UsingComparators' method", func(suite *internal.TestSuite) {
			usingComparatorsCalls := 0
			fnCall := &mockFnCall{mockedCall: &mockedCall{&mockMockCall{
				_UsingComparators: func(customComparators ...func(expected interface{}, received interface{}) bool) {
					usingComparatorsCalls += 1
				},
			}}}

			fnCall.UsingComparators()
			if usingComparatorsCalls != 1 {
				suite.Fatalf("The 'UsingComparators' method didn't correctly called the MockCall 'UsingComparators' method correctly")
			}
		})
		suite.Run("Should return itself", func(suite *internal.TestSuite) {
			fnCall := &mockFnCall{mockedCall: &mockedCall{&mockMockCall{
				_UsingComparators: func(customComparators ...func(expected interface{}, received interface{}) bool) {},
			}}}

			if fnCall.UsingComparators().(*mockFnCall) != fnCall {
				suite.Fatalf("The 'UsingComparators' method returned a wrong value")
			}
		})
		suite.Run("Should forward the correct comparators to the MockCall 'UsingComparators' method", func(suite *internal.TestSuite) {
			comparator0Calls := 0
			comparator0 := func(expected interface{}, received interface{}) bool {
				comparator0Calls += 1
				return false
			}
			comparator1Calls := 0
			comparator1 := func(expected interface{}, received interface{}) bool {
				comparator1Calls += 1
				return false
			}
			fnCall := &mockFnCall{mockedCall: &mockedCall{&mockMockCall{
				_UsingComparators: func(customComparators ...func(expected interface{}, received interface{}) bool) {
					if len(customComparators) != 2 {
						suite.Fatalf("The 'UsingComparators' didn't forwarded the correct comparators to the MockCall 'UsingComparators'")
					}

					customComparators[0](nil, nil)
					customComparators[1](nil, nil)
				},
			}}}
			fnCall.UsingComparators(comparator0, comparator1)
			if comparator0Calls != 1 || comparator1Calls != 1 {
				suite.Fatalf("The 'UsingComparators' didn't forwarded the correct comparators to the MockCall 'UsingComparators'")
			}
		})
	})
	suite.Run("Before", func(suite *internal.TestSuite) {
		suite.Run("Should call the MockSuite 'Helper' method", func(suite *internal.TestSuite) {
			helperCalls := 0
			mockSuite := &internal.MockSuite{Helper_: func() {
				helperCalls += 1
			}}
			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: mockSuite},
				mockedCall: &mockedCall{&mockMockCall{
					_Before: func(call mockCall.MockedCall, offsetInterval ...time.Duration) {},
				}},
			}
			fnCall.Before(&mockedCall{})

			if helperCalls != 1 {
				suite.Fatalf("The 'Before' method didn't called the MockSuite 'Helper' method")
			}
		})
		suite.Run("Should throw a failure if the given call is nil", func(suite *internal.TestSuite) {
			fatalfCalls := 0
			mockSuite := &internal.MockSuite{
				Fatalf_: func(msg string, args ...interface{}) {
					fatalfCalls += 1
					if msg != beforeNilCallError || len(args) != 0 {
						suite.Fatalf("The 'Before' method didn't correctly threw a failure message when received a nil MockedCall")
					}
				},
				Helper_: func() {},
			}
			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: mockSuite},
				mockedCall: &mockedCall{&mockMockCall{
					_Before: func(call mockCall.MockedCall, offsetInterval ...time.Duration) {},
				}},
			}
			fnCall.Before(nil)

			if fatalfCalls != 1 {
				suite.Fatalf("The 'Before' method didn't correctly threw a failure message when received a nil MockedCall")
			}
		})
		suite.Run("Should call the MockCall 'Before' method", func(suite *internal.TestSuite) {
			mockSuite := &internal.MockSuite{Helper_: func() {}}
			beforeCalls := 0
			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: mockSuite},
				mockedCall: &mockedCall{&mockMockCall{
					_Before: func(call mockCall.MockedCall, offsetInterval ...time.Duration) {
						beforeCalls += 1
					},
				}},
			}
			fnCall.Before(&mockedCall{})

			if beforeCalls != 1 {
				suite.Fatalf("The 'Before' method didn't correctly called the MockCall 'Before' method")
			}
		})
		suite.Run("Should return itself, providing a builder-like interface", func(suite *internal.TestSuite) {
			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: &internal.MockSuite{Helper_: func() {}}},
				mockedCall: &mockedCall{&mockMockCall{
					_Before: func(call mockCall.MockedCall, offsetInterval ...time.Duration) {},
				}},
			}
			if fnCall.Before(&mockedCall{}).(*mockFnCall) != fnCall {
				suite.Fatalf("The 'Before' method returned a wrong value")
			}
		})
		suite.Run("Should forward the given mock and offsetInterval to the MockCall 'Before' method", func(suite *internal.TestSuite) {
			otherCall := &mockedCall{&mockMockCall{}}
			offsetInterval := []time.Duration{time.Second, time.Second * 2}
			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: &internal.MockSuite{Helper_: func() {}}},
				mockedCall: &mockedCall{&mockMockCall{
					_Before: func(call mockCall.MockedCall, receivedOffset ...time.Duration) {
						if call.(*mockedCall) != otherCall || !reflect.DeepEqual(receivedOffset, offsetInterval) {
							suite.Fatalf("The 'Before' method didn't forwarded the correct arguments to the MockCall 'Before' method")
						}
					},
				}},
			}
			fnCall.Before(otherCall, offsetInterval...)
		})
	})
	suite.Run("BeforeAll", func(suite *internal.TestSuite) {
		suite.Run("Should call the 'Helper' MockSuite method", func(suite *internal.TestSuite) {
			helperCalls := 0
			mockSuite := &internal.MockSuite{Helper_: func() {
				helperCalls += 1
			}}
			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: mockSuite},
				mockedCall: &mockedCall{&mockMockCall{
					_Before: func(call mockCall.MockedCall, offsetInterval ...time.Duration) {},
				}},
			}

			fnCall.BeforeAll(&mockFn{})
			if helperCalls != 1 {
				suite.Fatalf("The 'BeforeAll' not called the MockSuite 'Helper' method")
			}
		})
		suite.Run("Should throw a failure if the given calls is nil", func(suite *internal.TestSuite) {
			fatalfCalls := 0
			mockSuite := &internal.MockSuite{Helper_: func() {}, Fatalf_: func(msg string, args ...interface{}) {
				fatalfCalls += 1
				if msg != beforeAllNilCallError {
					suite.Fatalf("The 'BeforeAll' method didn't threw the correct failure message when the calls argument is nil")
				}
				if len(args) != 0 {
					suite.Fatalf("The 'BeforeAll' method gave wrong arguments to the MockSuite 'Fatalf' method")
				}
			}}
			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: mockSuite},
				mockedCall: &mockedCall{&mockMockCall{
					_Before: func(call mockCall.MockedCall, offsetInterval ...time.Duration) {},
				}},
			}

			fnCall.BeforeAll(nil)
			if fatalfCalls != 1 {
				suite.Fatalf("The 'BeforeAll' not called the MockSuite 'Fatalf' method")
			}
		})
		suite.Run("Should call the 'Before' method of only with the first underlying call, since they share the same slice", func(suite *internal.TestSuite) {
			mockSuite := &internal.MockSuite{Helper_: func() {}}

			firstCall := &mockedCall{&mockMockCall{_Before: func(call mockCall.MockedCall, offsetInterval ...time.Duration) {}}}
			calls := &mockFn{
				suite:     mockSuite,
				syncCalls: []MockedCall{firstCall, &mockedCall{}, &mockedCall{}},
			}

			successfullCall := 0
			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: mockSuite},
				mockedCall: &mockedCall{&mockMockCall{
					_Before: func(call mockCall.MockedCall, offsetInterval ...time.Duration) {
						if call.(*mockedCall) != firstCall {
							suite.Fatalf("The 'BeforeAll' method is calling the 'Before' method with calls that aren't the first")
						}
						successfullCall += 1
					},
				}},
			}
			fnCall.BeforeAll(calls)

			if successfullCall != 1 {
				suite.Fatalf("The 'BeforeAll' method didn't called the 'Before' method with the first underlying call")
			}
		})
		suite.Run("Should return itself, providing a builder-like interface", func(suite *internal.TestSuite) {
			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: &internal.MockSuite{Helper_: func() {}}},
				mockedCall: &mockedCall{&mockMockCall{
					_Before: func(call mockCall.MockedCall, offsetInterval ...time.Duration) {},
				}},
			}
			if fnCall.BeforeAll(&mockFn{}).(*mockFnCall) != fnCall {
				suite.Fatalf("The 'BeforeAll' method returned a wrong value")
			}
		})
		suite.Run("Should forward the offsetInterval to the MockCall 'Before' method", func(suite *internal.TestSuite) {
			offsetInterval := []time.Duration{time.Second, time.Second * 2}
			mockSuite := &internal.MockSuite{Helper_: func() {}}
			successfullCall := 0
			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: mockSuite},
				mockedCall: &mockedCall{&mockMockCall{
					_Before: func(call mockCall.MockedCall, receivedOffset ...time.Duration) {
						if !reflect.DeepEqual(receivedOffset, offsetInterval) {
							suite.Fatalf("The 'BeforeAll' method didn't forwarded the correct arguments to the MockCall 'Before' method")
						}
						successfullCall += 1
					},
				}},
			}

			calls := &mockFn{suite: mockSuite, syncCalls: []MockedCall{nil, nil}}
			fnCall.BeforeAll(calls, offsetInterval...)
			if successfullCall != 1 {
				suite.Fatalf("The 'BeforeAll' method didn't called the 'Before' method correctly")
			}
		})
	})
	suite.Run("After", func(suite *internal.TestSuite) {
		suite.Run("Should call the MockSuite 'Helper' method", func(suite *internal.TestSuite) {
			helperCalls := 0
			mockSuite := &internal.MockSuite{Helper_: func() {
				helperCalls += 1
			}}
			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: mockSuite},
				mockedCall: &mockedCall{&mockMockCall{
					_After: func(call mockCall.MockedCall, offsetInterval ...time.Duration) {},
				}},
			}
			fnCall.After(&mockedCall{})

			if helperCalls != 1 {
				suite.Fatalf("The 'After' method didn't called the MockSuite 'Helper' method")
			}
		})
		suite.Run("Should throw a failure if the given call is nil", func(suite *internal.TestSuite) {
			fatalfCalls := 0
			mockSuite := &internal.MockSuite{
				Fatalf_: func(msg string, args ...interface{}) {
					fatalfCalls += 1
					if msg != afterNilCallError || len(args) != 0 {
						suite.Fatalf("The 'After' method didn't correctly threw a failure message when received a nil MockedCall")
					}
				},
				Helper_: func() {},
			}
			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: mockSuite},
				mockedCall: &mockedCall{&mockMockCall{
					_After: func(call mockCall.MockedCall, offsetInterval ...time.Duration) {},
				}},
			}
			fnCall.After(nil)

			if fatalfCalls != 1 {
				suite.Fatalf("The 'After' method didn't correctly threw a failure message when received a nil MockedCall")
			}
		})
		suite.Run("Should call the MockCall 'After' method", func(suite *internal.TestSuite) {
			mockSuite := &internal.MockSuite{Helper_: func() {}}
			afterCall := 0
			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: mockSuite},
				mockedCall: &mockedCall{&mockMockCall{
					_After: func(call mockCall.MockedCall, offsetInterval ...time.Duration) {
						afterCall += 1
					},
				}},
			}
			fnCall.After(&mockedCall{})

			if afterCall != 1 {
				suite.Fatalf("The 'After' method didn't correctly called the MockCall 'After' method")
			}
		})
		suite.Run("Should return itself, providing a builder-like interface", func(suite *internal.TestSuite) {
			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: &internal.MockSuite{Helper_: func() {}}},
				mockedCall: &mockedCall{&mockMockCall{
					_After: func(call mockCall.MockedCall, offsetInterval ...time.Duration) {},
				}},
			}
			if fnCall.After(&mockedCall{}).(*mockFnCall) != fnCall {
				suite.Fatalf("The 'After' method returned a wrong value")
			}
		})
		suite.Run("Should forward the given mock and offsetInterval to the MockCall 'After' method", func(suite *internal.TestSuite) {
			otherCall := &mockedCall{&mockMockCall{}}
			offsetInterval := []time.Duration{time.Second, time.Second * 2}
			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: &internal.MockSuite{Helper_: func() {}}},
				mockedCall: &mockedCall{&mockMockCall{
					_After: func(call mockCall.MockedCall, receivedOffset ...time.Duration) {
						if call.(*mockedCall) != otherCall || !reflect.DeepEqual(receivedOffset, offsetInterval) {
							suite.Fatalf("The 'After' method didn't forwarded the correct arguments to the MockCall 'After' method")
						}
					},
				}},
			}
			fnCall.After(otherCall, offsetInterval...)
		})
	})
	suite.Run("AfterAll", func(suite *internal.TestSuite) {
		suite.Run("Should call the 'Helper' MockSuite method", func(suite *internal.TestSuite) {
			helperCalls := 0
			mockSuite := &internal.MockSuite{Helper_: func() {
				helperCalls += 1
			}}
			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: mockSuite},
				mockedCall: &mockedCall{&mockMockCall{
					_After: func(call mockCall.MockedCall, offsetInterval ...time.Duration) {},
				}},
			}

			fnCall.AfterAll(&mockFn{})
			if helperCalls != 1 {
				suite.Fatalf("The 'AfterAll' not called the MockSuite 'Helper' method")
			}
		})
		suite.Run("Should throw a failure if the given calls is nil", func(suite *internal.TestSuite) {
			fatalfCalls := 0
			mockSuite := &internal.MockSuite{Helper_: func() {}, Fatalf_: func(msg string, args ...interface{}) {
				fatalfCalls += 1
				if msg != afterAllNilCallError {
					suite.Fatalf("The 'AfterAll' method didn't threw the correct failure message when the calls argument is nil")
				}
				if len(args) != 0 {
					suite.Fatalf("The 'AfterAll' method gave wrong arguments to the MockSuite 'Fatalf' method")
				}
			}}
			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: mockSuite},
				mockedCall: &mockedCall{&mockMockCall{
					_After: func(call mockCall.MockedCall, offsetInterval ...time.Duration) {},
				}},
			}

			fnCall.AfterAll(nil)
			if fatalfCalls != 1 {
				suite.Fatalf("The 'AfterAll' not called the MockSuite 'Fatalf' method")
			}
		})
		suite.Run("Should call the 'After' method with all the given calls", func(suite *internal.TestSuite) {
			mockSuite := &internal.MockSuite{Helper_: func() {}}

			call0, call1, call2 := &mockedCall{}, &mockedCall{}, &mockedCall{}
			call0Calls, call1Calls, call2Calls := 0, 0, 0
			calls := &mockFn{
				syncCalls: []MockedCall{call0, call1, call2},
			}

			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: mockSuite},
				mockedCall: &mockedCall{&mockMockCall{
					_After: func(call mockCall.MockedCall, offsetInterval ...time.Duration) {
						if call == nil {
							suite.Fatalf("The 'AfterAll' method is calling the 'After' method with wrong arguments")
						}

						if call.(*mockedCall) == call0 {
							call0Calls += 1
							return
						}
						if call.(*mockedCall) == call1 {
							call1Calls += 1
							return
						}
						if call.(*mockedCall) == call2 {
							call2Calls += 1
							return
						}
					},
				}},
			}
			fnCall.AfterAll(calls)

			if call0Calls != 1 || call1Calls != 1 || call2Calls != 1 {
				suite.Fatalf("The 'AfterAll' method didn't called the 'After' method for every underlying call")
			}
		})
		suite.Run("Should return itself, providing a builder-like interface", func(suite *internal.TestSuite) {
			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: &internal.MockSuite{Helper_: func() {}}},
				mockedCall: &mockedCall{&mockMockCall{
					_After: func(call mockCall.MockedCall, offsetInterval ...time.Duration) {},
				}},
			}
			if fnCall.AfterAll(&mockFn{}).(*mockFnCall) != fnCall {
				suite.Fatalf("The 'AfterAll' method returned a wrong value")
			}
		})
		suite.Run("Should forward the offsetInterval to the MockCall 'After' method", func(suite *internal.TestSuite) {
			offsetInterval := []time.Duration{time.Second, time.Second * 2}
			mockSuite := &internal.MockSuite{Helper_: func() {}}
			successfullCall := 0
			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: mockSuite},
				mockedCall: &mockedCall{&mockMockCall{
					_After: func(call mockCall.MockedCall, receivedOffset ...time.Duration) {
						if !reflect.DeepEqual(receivedOffset, offsetInterval) {
							suite.Fatalf("The 'AfterAll' method didn't forwarded the correct arguments to the MockCall 'After' method")
						}
						successfullCall += 1
					},
				}},
			}

			calls := &mockFn{suite: mockSuite, syncCalls: []MockedCall{nil, nil}}
			fnCall.AfterAll(calls, offsetInterval...)
			if successfullCall < len(calls.syncCalls) {
				suite.Fatalf("The 'AfterAll' method didn't called the 'After' method correctly")
			}
		})
	})
	suite.Run("Waiting", func(suite *internal.TestSuite) {
		suite.Run("Should return itself, providing a builder-like interface", func(suite *internal.TestSuite) {
			fnCall := &mockFnCall{
				mockFn: &mockFn{suite: &internal.MockSuite{Helper_: func() {}}},
				mockedCall: &mockedCall{&mockMockCall{
					_Waiting: func(timeout time.Duration) {},
				}},
			}
			if fnCall.Waiting(time.Second).(*mockFnCall) != fnCall {
				suite.Fatalf("The 'AfterAll' method returned a wrong value")
			}
		})
		suite.Run("Should forward the timeout argument to the MockCall 'Waiting' method", func(suite *internal.TestSuite) {
			timeout := time.Second
			waitingCalls := 0
			fnCall := &mockFnCall{
				mockedCall: &mockedCall{&mockMockCall{
					_Waiting: func(receivedTimeout time.Duration) {
						waitingCalls += 1
						if receivedTimeout != timeout {
							suite.Fatalf("The 'Waiting' method forwarded the wrong timeout to the MockCall 'Waiting' method")
						}
					},
				}},
			}

			fnCall.Waiting(timeout)
			if waitingCalls != 1 {
				suite.Fatalf("The 'Waiting' method didn't called the MockCall 'Waiting' method")
			}
		})
	})
	suite.Run("Bind", func(suite *internal.TestSuite) {
		suite.Run("Should call the MockSuite 'Helper' method", func(suite *internal.TestSuite) {
			helperCalls := 0
			mockSuite := &internal.MockSuite{Helper_: func() {
				helperCalls += 1
			}}
			fnCall := &mockFnCall{mockFn: &mockFn{suite: mockSuite}}

			var call MockedCall
			fnCall.Bind(&call)
			if helperCalls != 1 {
				suite.Fatalf("The 'Bind' method not called the MockSuite 'Helper' method")
			}
		})
		suite.Run("Should throw a failure if the given *MockedCall is nil", func(suite *internal.TestSuite) {
			fatalfCalls := 0
			mockSuite := &internal.MockSuite{Helper_: func() {}, Fatalf_: func(msg string, args ...interface{}) {
				fatalfCalls += 1
				if msg != bindNilMockedCallError {
					suite.Fatalf("The 'Bind' method threw the wrong failure message when receiving nil *MockedCall")
				}
				if len(args) != 0 {
					suite.Fatalf("The 'Bind' method gave the wrong arguments to the MockSuite 'Fatalf' method")
				}
			}}
			fnCall := &mockFnCall{mockFn: &mockFn{suite: mockSuite}}

			fnCall.Bind(nil)
			if fatalfCalls != 1 {
				suite.Fatalf("The 'Bind' method not called the MockSuite 'Fatalf' method when receiving a nil *MockedCall")
			}
		})
		suite.Run("Should correctly fill the given pointer with the mockFn itself (since it satisfies the interface)", func(suite *internal.TestSuite) {
			mockSuite := &internal.MockSuite{Helper_: func() {}}
			fnCall := &mockFnCall{mockFn: &mockFn{suite: mockSuite}}

			var token MockedCall
			fnCall.Bind(&token)
			if token == nil || token.(*mockFnCall) != fnCall {
				suite.Fatalf("The 'Bind' method didn't filled the given pointer with the correct MockedCalls value")
			}
		})
		suite.Run("Should return itself, providing a builder-like interface", func(suite *internal.TestSuite) {
			mockSuite := &internal.MockSuite{Helper_: func() {}}
			fnCall := &mockFnCall{mockFn: &mockFn{suite: mockSuite}}

			var call MockedCall
			if fnCall.Bind(&call).(*mockFnCall) != fnCall {
				suite.Fatalf("The 'Bind' method returned a wrong value")
			}
		})
	})
	suite.Run("Repeat", func(suite *internal.TestSuite) {
		suite.Run("Should call the MockSuite 'Helper' method", func(suite *internal.TestSuite) {
			helperCalls := 0
			mockSuite := &internal.MockSuite{
				Fatalf_: func(msg string, args ...interface{}) {}, // avoid nil pointer
				Helper_: func() { helperCalls += 1 },
			}
			fnCall := &mockFnCall{mockFn: &mockFn{suite: mockSuite}}

			fnCall.Repeat(0) // expect that values < 2 throw failure
			if helperCalls != 1 {
				suite.Fatalf("The 'Repeat' method not called the MockSuite 'Helper' method")
			}
		})
		suite.Run("Should throw a failure if the argument is lesser than two", func(suite *internal.TestSuite) {
			fatalfCalls := 0
			mockSuite := &internal.MockSuite{
				Fatalf_: func(msg string, args ...interface{}) {
					fatalfCalls += 1
					if msg != repeatNotPluralError {
						suite.Fatalf("The 'Repeat' method used the wrong failure message to signal that the argument must be greater than two")
					}
					if len(args) != 0 {
						suite.Fatalf("The 'Repeat' method gave arguments to the MockSuite 'Fatalf' method")
					}
				},
				Helper_: func() {},
			}
			mockedCall := &mockedCall{&mockMockCall{
				// Avoids nil pointer
				_Copy: func() *mockCall.MockCall {
					return nil
				},
			}}
			fnCall := &mockFnCall{
				mockFn:     &mockFn{suite: mockSuite},
				mockedCall: mockedCall,
			}

			fnCall.Repeat(0)
			fnCall.Repeat(1)
			fnCall.Repeat(2)
			if fatalfCalls != 2 {
				suite.Fatalf("The 'Repeat' method didn't called the MockSuite 'Fatalf' correctly with arguments lesser than 2")
			}
		})
		suite.Run("The current call must be copied n-1 times, since the mockFn will register the first by itself", func(suite *internal.TestSuite) {
			var copiedMockedCalls []*mockedCall
			originalMockedCall := &mockedCall{&mockMockCall{_Copy: func() *mockCall.MockCall {
				newCopy := &mockCall.MockCall{}
				copiedMockedCalls = append(copiedMockedCalls, &mockedCall{newCopy})
				return newCopy
			}}}
			fn := &mockFn{
				suite: &internal.MockSuite{Helper_: func() {}},

				// The expected behaviour of the mockFn is to already have saved the original call
				syncCalls: []MockedCall{originalMockedCall},
			}
			call := &mockFnCall{
				mockFn:     fn,
				mockedCall: originalMockedCall,
			}

			times := uint(2)
			call.Repeat(times)
			if len(fn.syncCalls) != int(times) {
				suite.Fatalf("The 'Repeat' method didn't copied the calls correctly")
			}
			if fn.syncCalls[0].(*mockedCall) != originalMockedCall {
				suite.Fatalf("The 'Repeat' method didn't copied the calls correctly")
			}
			for i := 1; i < int(times); i++ {
				if fn.syncCalls[i].(*mockedCall).iMockCall.(*mockCall.MockCall) != copiedMockedCalls[i-1].iMockCall.(*mockCall.MockCall) {
					suite.Fatalf("The 'Repeat' method didn't copied the calls correctly")
				}
			}
		})
		suite.Run("Should return the underlying *mockFn to restrict the builder-like interface", func(suite *internal.TestSuite) {
			mockSuite := &internal.MockSuite{Helper_: func() {}}
			fn := &mockFn{suite: mockSuite}
			fnCall := &mockFnCall{
				mockFn: fn,
				mockedCall: &mockedCall{&mockMockCall{_Copy: func() *mockCall.MockCall {
					return nil
				}}},
			}

			if fnCall.Repeat(2).(*mockFn) != fn {
				suite.Fatalf("The 'Repeat' method didn't returned the correct value")
			}
		})
	})
	suite.Run("The 'calls' method should return a singleton containing the call itself", func(suite *internal.TestSuite) {
		fnCall := &mockFnCall{
			mockFn: &mockFn{
				syncCalls: []MockedCall{nil, nil},
			},
		}

		calls := fnCall.calls()
		if len(calls) != 1 || calls[0].(*mockFnCall) != fnCall {
			suite.Fatalf("The 'calls' method is not returning a singleton with itself")
		}
	})
}

func TestMockFnCallArgs(t *testing.T) {
	suite := &internal.TestSuite{t}

	// It is not worth to test that every method of the mockFnCallArgs is calling the correspondent
	// mockFnCall method

	suite.Run("The 'Before' method should call the MockSuite 'Helper' method", func(suite *internal.TestSuite) {
		helperCalls := 0
		mockSuite := &internal.MockSuite{
			Fatalf_: func(msg string, args ...interface{}) {}, // avoid nil pointer
			Helper_: func() { helperCalls += 1 },
		}
		fnCallArgs := &mockFnCallArgs{&mockFnCall{mockFn: &mockFn{suite: mockSuite}}}

		fnCallArgs.Before(nil) // expect that nil values throw failure
		if helperCalls != 2 {  // expects that the underlying mockFnCall will call the 'Helper' method too
			suite.Fatalf("The 'Before' method not called the MockSuite 'Helper' method")
		}
	})
	suite.Run("The 'BeforeAll' method should call the MockSuite 'Helper' method", func(suite *internal.TestSuite) {
		helperCalls := 0
		mockSuite := &internal.MockSuite{
			Fatalf_: func(msg string, args ...interface{}) {}, // avoid nil pointer
			Helper_: func() { helperCalls += 1 },
		}
		fnCallArgs := &mockFnCallArgs{&mockFnCall{mockFn: &mockFn{suite: mockSuite}}}

		fnCallArgs.BeforeAll(nil) // expect that nil values throw failure
		if helperCalls != 2 {     // expects that the underlying mockFnCall will call the 'Helper' method too
			suite.Fatalf("The 'BeforeAll' method not called the MockSuite 'Helper' method")
		}
	})
	suite.Run("The 'After' method should call the MockSuite 'Helper' method", func(suite *internal.TestSuite) {
		helperCalls := 0
		mockSuite := &internal.MockSuite{
			Fatalf_: func(msg string, args ...interface{}) {}, // avoid nil pointer
			Helper_: func() { helperCalls += 1 },
		}
		fnCallArgs := &mockFnCallArgs{&mockFnCall{mockFn: &mockFn{suite: mockSuite}}}

		fnCallArgs.After(nil) // expect that nil values throw failure
		if helperCalls != 2 { // expects that the underlying mockFnCall will call the 'Helper' method too
			suite.Fatalf("The 'After' method not called the MockSuite 'Helper' method")
		}
	})
	suite.Run("The 'AfterAll' method should call the MockSuite 'Helper' method", func(suite *internal.TestSuite) {
		helperCalls := 0
		mockSuite := &internal.MockSuite{
			Fatalf_: func(msg string, args ...interface{}) {}, // avoid nil pointer
			Helper_: func() { helperCalls += 1 },
		}
		fnCallArgs := &mockFnCallArgs{&mockFnCall{mockFn: &mockFn{suite: mockSuite}}}

		fnCallArgs.AfterAll(nil) // expect that nil values throw failure
		if helperCalls != 2 {    // expects that the underlying mockFnCall will call the 'Helper' method too
			suite.Fatalf("The 'AfterAll' method not called the MockSuite 'Helper' method")
		}
	})
}
