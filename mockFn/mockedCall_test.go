package mockFn

type mockMockedCall struct {
	wait_         func()
	matchArgs_    func(...interface{}) bool
	expectedArgs_ func() []interface{}
	call_         func(...interface{}) []interface{}
	called_       func() bool
	MockedCall
}

func (m *mockMockedCall) wait()                                  { m.wait_() }
func (m *mockMockedCall) matchArgs(args ...interface{}) bool     { return m.matchArgs_(args...) }
func (m *mockMockedCall) expectedArgs() []interface{}            { return m.expectedArgs_() }
func (m *mockMockedCall) call(args ...interface{}) []interface{} { return m.call_(args...) }
func (m *mockMockedCall) called() bool                           { return m.called_() }
