package mockFn

import (
	"gitlab.com/loxe-tools/go-test/internal/mockCall"
	"time"
)

type mockMockCall struct {
	_IgnoringArgs     func()
	_Expecting        func(expectedArgs ...interface{})
	_AsStub           func(stubResults ...interface{})
	_Returning        func(functionResults ...interface{})
	_UsingComparators func(customComparators ...func(expected, received interface{}) bool)
	_Before           func(call mockCall.MockedCall, offsetInterval ...time.Duration)
	_After            func(call mockCall.MockedCall, offsetInterval ...time.Duration)
	_Waiting          func(timeout time.Duration)
	_ExpectedArgs     func() []interface{}
	_Called           func() bool
	_Wait             func()
	_MatchArgs        func(receivedArgs ...interface{}) bool
	_Call             func(receivedArgs ...interface{}) []interface{}
	_Copy             func() *mockCall.MockCall
	mockCall.MockedCall
}

func (m *mockMockCall) IgnoringArgs() {
	m._IgnoringArgs()
}
func (m *mockMockCall) Expecting(expectedArgs ...interface{}) {
	m._Expecting(expectedArgs...)
}
func (m *mockMockCall) AsStub(stubResults ...interface{}) {
	m._AsStub(stubResults...)
}
func (m *mockMockCall) Returning(functionResults ...interface{}) {
	m._Returning(functionResults...)
}
func (m *mockMockCall) UsingComparators(customComparators ...func(expected, received interface{}) bool) {
	m._UsingComparators(customComparators...)
}
func (m *mockMockCall) Before(call mockCall.MockedCall, offsetInterval ...time.Duration) {
	m._Before(call, offsetInterval...)
}
func (m *mockMockCall) After(call mockCall.MockedCall, offsetInterval ...time.Duration) {
	m._After(call, offsetInterval...)
}
func (m *mockMockCall) Waiting(timeout time.Duration) {
	m._Waiting(timeout)
}
func (m *mockMockCall) ExpectedArgs() []interface{} {
	return m._ExpectedArgs()
}
func (m *mockMockCall) Called() bool {
	return m._Called()
}
func (m *mockMockCall) Wait() {
	m._Wait()
}
func (m *mockMockCall) MatchArgs(receivedArgs ...interface{}) bool {
	return m._MatchArgs(receivedArgs...)
}
func (m *mockMockCall) Call(receivedArgs ...interface{}) []interface{} {
	return m._Call(receivedArgs...)
}
func (m *mockMockCall) Copy() *mockCall.MockCall {
	return m._Copy()
}
