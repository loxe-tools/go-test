package mockFn

import (
	"gitlab.com/loxe-tools/go-test/internal"
	"gitlab.com/loxe-tools/go-test/internal/mockCall"
	"testing"
)

func TestMockFnMultiCall(t *testing.T) {
	suite := &internal.TestSuite{t}

	// It is not worth to test that every method of the mockMultiFnCall is calling the correspondent
	// mockFnCall method

	suite.Run("The 'Before' method should call the MockSuite 'Helper' method", func(suite *internal.TestSuite) {
		helperCalls := 0
		mockSuite := &internal.MockSuite{
			Fatalf_: func(msg string, args ...interface{}) {}, // avoid nil pointer
			Helper_: func() { helperCalls += 1 },
		}
		fnMultiCall := &mockFnMultiCall{&mockFnCall{mockFn: &mockFn{suite: mockSuite}}}

		fnMultiCall.Before(nil) // expect that nil values throw failure
		if helperCalls != 2 {  // expects that the underlying mockFnCall will call the 'Helper' method too
			suite.Fatalf("The 'Before' method not called the MockSuite 'Helper' method")
		}
	})
	suite.Run("The 'BeforeAll' method should call the MockSuite 'Helper' method", func(suite *internal.TestSuite) {
		helperCalls := 0
		mockSuite := &internal.MockSuite{
			Fatalf_: func(msg string, args ...interface{}) {}, // avoid nil pointer
			Helper_: func() { helperCalls += 1 },
		}
		fnMultiCall := &mockFnMultiCall{&mockFnCall{mockFn: &mockFn{suite: mockSuite}}}

		fnMultiCall.BeforeAll(nil) // expect that nil values throw failure
		if helperCalls != 2 {     // expects that the underlying mockFnCall will call the 'Helper' method too
			suite.Fatalf("The 'BeforeAll' method not called the MockSuite 'Helper' method")
		}
	})
	suite.Run("The 'After' method should call the MockSuite 'Helper' method", func(suite *internal.TestSuite) {
		helperCalls := 0
		mockSuite := &internal.MockSuite{
			Fatalf_: func(msg string, args ...interface{}) {}, // avoid nil pointer
			Helper_: func() { helperCalls += 1 },
		}
		fnMultiCall := &mockFnMultiCall{&mockFnCall{mockFn: &mockFn{suite: mockSuite}}}

		fnMultiCall.After(nil) // expect that nil values throw failure
		if helperCalls != 2 { // expects that the underlying mockFnCall will call the 'Helper' method too
			suite.Fatalf("The 'After' method not called the MockSuite 'Helper' method")
		}
	})
	suite.Run("The 'AfterAll' method should call the MockSuite 'Helper' method", func(suite *internal.TestSuite) {
		helperCalls := 0
		mockSuite := &internal.MockSuite{
			Fatalf_: func(msg string, args ...interface{}) {}, // avoid nil pointer
			Helper_: func() { helperCalls += 1 },
		}
		fnMultiCall := &mockFnMultiCall{&mockFnCall{mockFn: &mockFn{suite: mockSuite}}}

		fnMultiCall.AfterAll(nil) // expect that nil values throw failure
		if helperCalls != 2 {    // expects that the underlying mockFnCall will call the 'Helper' method too
			suite.Fatalf("The 'AfterAll' method not called the MockSuite 'Helper' method")
		}
	})
	suite.Run("Bind", func(suite *internal.TestSuite) {
		suite.Run("Should call the MockSuite 'Helper' method", func(suite *internal.TestSuite) {
			helperCalls := 0
			mockSuite := &internal.MockSuite{
				Fatalf_: func(msg string, args ...interface{}) {}, // avoid nil pointer
				Helper_: func() { helperCalls += 1 },
			}
			fnMultiCall := &mockFnMultiCall{&mockFnCall{mockFn: &mockFn{suite: mockSuite}}}

			fnMultiCall.Bind(nil) // expect that nil values throw failure
			if helperCalls != 2 { // expects that the underlying mockFnCall will call the 'Helper' method too
				suite.Fatalf("The 'Bind' method not called the MockSuite 'Helper' method")
			}
		})
		suite.Run("Should return a singleton containing the call itself", func(suite *internal.TestSuite) {
			mockSuite := &internal.MockSuite{Helper_: func() {}}
			fnMultiCall := &mockFnMultiCall{&mockFnCall{mockFn: &mockFn{suite: mockSuite}}}

			var token MockedCall
			calls := fnMultiCall.Bind(&token).calls()
			if len(calls) != 1 || calls[0].(*mockFnCall) != fnMultiCall.mockFnCall {
				suite.Fatalf("The 'calls' method is not returning a singleton with itself")
			}
		})
	})
	suite.Run("Repeat", func(suite *internal.TestSuite) {
		suite.Run("Should call the MockSuite 'Helper' method", func(suite *internal.TestSuite) {
			helperCalls := 0
			mockSuite := &internal.MockSuite{
				Fatalf_: func(msg string, args ...interface{}) {}, // avoid nil pointer
				Helper_: func() { helperCalls += 1 },
			}
			fnMultiCall := &mockFnMultiCall{&mockFnCall{mockFn: &mockFn{suite: mockSuite}}}

			fnMultiCall.Repeat(0) // expect that values < 2 throw failure
			if helperCalls != 1 {
				suite.Fatalf("The 'Repeat' method not called the MockSuite 'Helper' method")
			}
		})
		suite.Run("Should throw a failure if the argument is lesser than two", func(suite *internal.TestSuite) {
			fatalfCalls := 0
			mockSuite := &internal.MockSuite{
				Fatalf_: func(msg string, args ...interface{}) {
					fatalfCalls += 1
					if msg != repeatNotPluralError {
						suite.Fatalf("The 'Repeat' method used the wrong failure message to signal that the argument must be greater than two")
					}
					if len(args) != 0 {
						suite.Fatalf("The 'Repeat' method gave arguments to the MockSuite 'Fatalf' method")
					}
				},
				Helper_: func() {},
			}
			fnMultiCall := &mockFnMultiCall{
				&mockFnCall{
					mockFn: &mockFn{suite: mockSuite},
					mockedCall: &mockedCall{&mockMockCall{
						// Avoids nil pointer
						_Copy: func() *mockCall.MockCall {
							return nil
						},
					}},
				},
			}

			fnMultiCall.Repeat(0)
			fnMultiCall.Repeat(1)
			fnMultiCall.Repeat(2)
			if fatalfCalls != 2 {
				suite.Fatalf("The 'Repeat' method didn't called the MockSuite 'Fatalf' correctly with arguments lesser than 2")
			}
		})
		suite.Run("The current call must be copied n times inside a new slice and returned as MockFnMultiCalls and the 'syncCalls' field of the mockFn leave untouched", func(suite *internal.TestSuite) {
			var copiedMockedCalls []*mockCall.MockCall
			originalMockedCall := &mockedCall{&mockMockCall{_Copy: func() *mockCall.MockCall {
				newCopy := &mockCall.MockCall{}
				copiedMockedCalls = append(copiedMockedCalls, newCopy)
				return newCopy
			}}}
			call := &mockFnMultiCall{
				&mockFnCall{
					mockFn: &mockFn{
						suite:     &internal.MockSuite{Helper_: func() {}},
						syncCalls: nil,
					},
					mockedCall: originalMockedCall,
				},
			}

			times := uint(2)
			calls := call.Repeat(times).calls()
			if len(calls) != int(times) {
				suite.Fatalf("The 'Repeat' method didn't copied the correct number of calls")
			}
			if calls[0].(*mockFnMultiCall).mockedCall != originalMockedCall {
				suite.Fatalf("The 'Repeat' method didn't copied the calls correctly")
			}
			for i := 1; i < int(times); i++ {
				if calls[i].(*mockedCall).iMockCall.(*mockCall.MockCall) != copiedMockedCalls[i-1] {
					suite.Fatalf("The 'Repeat' method didn't copied the calls correctly")
				}
			}
		})
	})
}

func TestMockFnMultiCallArgs(t *testing.T) {
	suite := &internal.TestSuite{t}

	// It is not worth to test that every method of the mockFnMultiCallArgs is calling the correspondent
	// mockFnCall method

	suite.Run("The 'Before' method should call the MockSuite 'Helper' method", func(suite *internal.TestSuite) {
		helperCalls := 0
		mockSuite := &internal.MockSuite{
			Fatalf_: func(msg string, args ...interface{}) {}, // avoid nil pointer
			Helper_: func() { helperCalls += 1 },
		}
		fnMultiCallArgs := &mockFnMultiCallArgs{&mockFnMultiCall{&mockFnCall{mockFn: &mockFn{suite: mockSuite}}}}

		fnMultiCallArgs.Before(nil) // expect that nil values throw failure
		if helperCalls != 2 {  // expects that the underlying mockFnCall will call the 'Helper' method too
			suite.Fatalf("The 'Before' method not called the MockSuite 'Helper' method")
		}
	})
	suite.Run("The 'BeforeAll' method should call the MockSuite 'Helper' method", func(suite *internal.TestSuite) {
		helperCalls := 0
		mockSuite := &internal.MockSuite{
			Fatalf_: func(msg string, args ...interface{}) {}, // avoid nil pointer
			Helper_: func() { helperCalls += 1 },
		}
		fnMultiCallArgs := &mockFnMultiCallArgs{&mockFnMultiCall{&mockFnCall{mockFn: &mockFn{suite: mockSuite}}}}

		fnMultiCallArgs.BeforeAll(nil) // expect that nil values throw failure
		if helperCalls != 2 {     // expects that the underlying mockFnCall will call the 'Helper' method too
			suite.Fatalf("The 'BeforeAll' method not called the MockSuite 'Helper' method")
		}
	})
	suite.Run("The 'After' method should call the MockSuite 'Helper' method", func(suite *internal.TestSuite) {
		helperCalls := 0
		mockSuite := &internal.MockSuite{
			Fatalf_: func(msg string, args ...interface{}) {}, // avoid nil pointer
			Helper_: func() { helperCalls += 1 },
		}
		fnMultiCallArgs := &mockFnMultiCallArgs{&mockFnMultiCall{&mockFnCall{mockFn: &mockFn{suite: mockSuite}}}}

		fnMultiCallArgs.After(nil) // expect that nil values throw failure
		if helperCalls != 2 { // expects that the underlying mockFnCall will call the 'Helper' method too
			suite.Fatalf("The 'After' method not called the MockSuite 'Helper' method")
		}
	})
	suite.Run("The 'AfterAll' method should call the MockSuite 'Helper' method", func(suite *internal.TestSuite) {
		helperCalls := 0
		mockSuite := &internal.MockSuite{
			Fatalf_: func(msg string, args ...interface{}) {}, // avoid nil pointer
			Helper_: func() { helperCalls += 1 },
		}
		fnMultiCallArgs := &mockFnMultiCallArgs{&mockFnMultiCall{&mockFnCall{mockFn: &mockFn{suite: mockSuite}}}}

		fnMultiCallArgs.AfterAll(nil) // expect that nil values throw failure
		if helperCalls != 2 {    // expects that the underlying mockFnCall will call the 'Helper' method too
			suite.Fatalf("The 'AfterAll' method not called the MockSuite 'Helper' method")
		}
	})
}
