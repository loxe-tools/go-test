package mockFn

import (
	"fmt"
	"strings"
)

func (m *mockFn) Call(receivedArgs ...interface{}) []interface{} {
	m.suite.Helper()
	totalCalls := len(m.syncCalls) + len(m.asyncCalls)
	if m.nextCall >= totalCalls {
		m.suite.Fatalf(unexpectedCallError, displayArgs(receivedArgs...), m.fileLine)
		return nil
	}

	m.mutex.Lock()
	defer func() {
		m.nextCall += 1
		m.mutex.Unlock()
	}()

	if m.nextCall < len(m.syncCalls) {
		nextOrderedCall := m.syncCalls[m.nextCall]

		if !nextOrderedCall.matchArgs(receivedArgs...) {
			m.suite.Fatalf(wrongNextOrderedCallError,
				displayArgs(receivedArgs...), displayArgs(nextOrderedCall.expectedArgs()...), m.fileLine)
			return nil
		}
		return nextOrderedCall.call(receivedArgs...)
	}

	for i := 0; i < len(m.asyncCalls); i++ {
		currAsyncCall := m.asyncCalls[i]

		if currAsyncCall.called() || !currAsyncCall.matchArgs(receivedArgs...) {
			continue
		}
		return currAsyncCall.call(receivedArgs...)
	}

	m.suite.Fatalf(unexpectedAsyncCallError, displayArgs(receivedArgs...), m.fileLine)
	return nil
}

// -----

func displayArgs(args ...interface{}) string {
	str := "func("
	for _, currArg := range args {
		str += fmt.Sprintf("%+v, ", currArg)
	}
	str = strings.TrimSuffix(str, ", ")

	return str + ")"
}

const unexpectedCallError = `There's an unexpected extra mock function call:
Arguments:
	%v
Mock:
	%s`

const wrongNextOrderedCallError = `The following call doesn't matches the next expected ordered call:
Received:
	Arguments:
		%v
Expected:
	Arguments:
		%v
	Mocked at:
		%s`

const unexpectedAsyncCallError = `The following call doesn't matches none of the expected asynchronous calls:
Arguments:
	%v
Mock:
	%s`
