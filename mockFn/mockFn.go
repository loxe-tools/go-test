package mockFn

import (
	"gitlab.com/loxe-tools/go-test/internal"
	"gitlab.com/loxe-tools/go-test/internal/mockCall"
	"sync"
)

type MockFnAux interface {
	AsyncCalls(callback func(builder MockFnMulti) NonSequentialCalls) MockFnCalls
	MockFnCalls
}
type MockFn interface {
	IgnoringArgs() MockFnCallArgs
	Expecting(expectedArgs ...interface{}) MockFnCall
	AsStub(stubResults ...interface{}) MockFnStub
	SyncCalls(callback func(builder MockFnMulti) []MockedCalls) MockFnAux
	AsyncCalls(callback func(builder MockFnMulti) NonSequentialCalls) MockFnCalls
}

func NewMockFn(s MockSuite, skipFrames ...uint) MockFn {
	mockSkipFrame := uint(1)
	if len(skipFrames) > 0 {
		mockSkipFrame += skipFrames[0]
	}

	newMockFn := &mockFn{
		s,
		internal.FnCallFileLine(mockSkipFrame),
		mockSkipFrame,
		nil,
		nil,
		0,
		&sync.Mutex{},
		func(s MockSuite, skipFrames ...uint) iMockCall { return mockCall.New(s, skipFrames...) },
		false,
	}
	s.Cleanup(newMockFn.cleanupChecks)
	return newMockFn
}

// -----

type mockFn struct {
	suite           MockSuite
	fileLine        string
	mockSkipFrame   uint
	syncCalls       []MockedCall
	asyncCalls      []MockedCall
	nextCall        int
	mutex           mutex
	factory         func(suite MockSuite, skipFrames ...uint) iMockCall
	syncCallsCalled bool
}

func (m *mockFn) IgnoringArgs() MockFnCallArgs {
	newMockCall := &mockedCall{m.factory(m.suite, m.mockSkipFrame+1)}
	newMockCall.IgnoringArgs()

	m.syncCalls = []MockedCall{newMockCall}
	m.asyncCalls = nil
	m.syncCallsCalled = false
	return &mockFnCallArgs{&mockFnCall{m, newMockCall}}
}
func (m *mockFn) Expecting(expectedArgs ...interface{}) MockFnCall {
	newMockCall := &mockedCall{m.factory(m.suite, m.mockSkipFrame+1)}
	newMockCall.Expecting(expectedArgs...)

	m.syncCalls = []MockedCall{newMockCall}
	m.asyncCalls = nil
	m.syncCallsCalled = false
	return &mockFnCall{m, newMockCall}
}
func (m *mockFn) AsStub(stubResults ...interface{}) MockFnStub {
	newMockCall := &mockedCall{m.factory(m.suite, m.mockSkipFrame+1)}
	newMockCall.AsStub(stubResults...)

	m.syncCalls = []MockedCall{newMockCall}
	m.asyncCalls = nil
	m.syncCallsCalled = false
	return &mockFnStub{&mockFnCall{m, newMockCall}}
}
func (m *mockFn) SyncCalls(callback func(builder MockFnMulti) SequentialCalls) MockFnAux {
	m.suite.Helper()
	if callback == nil {
		m.suite.Fatalf(syncNilCallbackError)
		return nil
	}

	m.syncCalls = nil
	m.asyncCalls = nil
	calls := callback(&mockFnMulti{m})
	if len(calls) < 2 {
		m.suite.Fatalf(syncSingleCallError)
		return nil
	}

	for _, currOtherCall := range calls {
		m.syncCalls = append(m.syncCalls, currOtherCall.calls()...)
	}
	m.syncCallsCalled = true
	return m
}
func (m *mockFn) AsyncCalls(callback func(builder MockFnMulti) NonSequentialCalls) MockFnCalls {
	m.suite.Helper()
	if callback == nil {
		m.suite.Fatalf(asyncNilCallbackError)
		return nil
	}

	if !m.syncCallsCalled {
		m.syncCalls = nil
	}
	m.asyncCalls = nil
	calls := callback(&mockFnMulti{m})
	if len(calls) < 2 {
		m.suite.Fatalf(asyncSingleCallError)
		return nil
	}

	for _, currOtherCall := range calls {
		m.asyncCalls = append(m.asyncCalls, currOtherCall.calls()...)
	}
	return m
}
func (m *mockFn) Bind(token *MockedCalls) MockedFn {
	m.suite.Helper()
	if token == nil {
		m.suite.Fatalf(bindNilMockedCallError)
		return nil
	}

	*token = m
	return m
}
func (m *mockFn) cleanupChecks() {
	for _, currOrderedCall := range m.syncCalls {
		currOrderedCall.wait()
	}
	for _, currAsyncCall := range m.asyncCalls {
		currAsyncCall.wait()
	}
}
func (m *mockFn) calls() []MockedCall {
	return append(m.syncCalls, m.asyncCalls...)
}

type MockSuite interface {
	Helper()
	Cleanup(func())
	Fatalf(string, ...interface{})
}
type mutex interface {
	Lock()
	Unlock()
}
type SequentialCalls = []MockedCalls
type NonSequentialCalls = []MockedCalls

// -----

const syncSingleCallError = `You are returning just a single call via the SyncCalls method callback. Just use one of the single call methods`
const syncNilCallbackError = `You cannot pass a nil callback to the SyncCalls method`
const asyncSingleCallError = `You are returning just a single call via the AsyncCalls method callback. Just use one of the single call methods`
const asyncNilCallbackError = `You cannot pass a nil callback to the AsyncCalls method`
const bindNilMockedCallError = `Please, give a non-nil argument to the 'Bind' method`
