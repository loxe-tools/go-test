package mockFn

import (
	"time"
)

type MockFnCall interface {
	Returning(functionResults ...interface{}) MockFnCall
	UsingComparators(customComparators ...func(expected, received interface{}) bool) MockFnCall
	Before(call MockedCall, offsetInterval ...time.Duration) MockFnCall
	BeforeAll(call MockedCalls, offsetInterval ...time.Duration) MockFnCall
	After(call MockedCall, offsetInterval ...time.Duration) MockFnCall
	AfterAll(call MockedCalls, offsetInterval ...time.Duration) MockFnCall
	Waiting(timeout time.Duration) MockFnCall
	Bind(token *MockedCall) MockedFn
	Repeat(times uint) MockFnCalls
	MockedFn
}
type mockFnCall struct {
	*mockFn
	*mockedCall
}

func (w *mockFnCall) Returning(functionResults ...interface{}) MockFnCall {
	w.iMockCall.Returning(functionResults...)
	return w
}
func (w *mockFnCall) UsingComparators(customComparators ...func(expected, received interface{}) bool) MockFnCall {
	w.iMockCall.UsingComparators(customComparators...)
	return w
}
func (w *mockFnCall) Before(call MockedCall, offsetInterval ...time.Duration) MockFnCall {
	w.suite.Helper()
	if call == nil {
		w.suite.Fatalf(beforeNilCallError)
		return nil
	}

	w.iMockCall.Before(call, offsetInterval...)
	return w
}
func (w *mockFnCall) BeforeAll(calls MockedCalls, offsetInterval ...time.Duration) MockFnCall {
	w.suite.Helper()
	if calls == nil {
		w.suite.Fatalf(beforeAllNilCallError)
		return nil
	}

	for _, currCall := range calls.calls() {
		w.iMockCall.Before(currCall, offsetInterval...)

		// It's safe to call just one time, because all the MockedCalls share the same
		// slice of previous calls. Check internal implementation/tests for details
		// (MockCall Before and Copy methods)
		break
	}
	return w
}
func (w *mockFnCall) After(call MockedCall, offsetInterval ...time.Duration) MockFnCall {
	w.suite.Helper()
	if call == nil {
		w.suite.Fatalf(afterNilCallError)
		return nil
	}

	w.iMockCall.After(call, offsetInterval...)
	return w
}
func (w *mockFnCall) AfterAll(calls MockedCalls, offsetInterval ...time.Duration) MockFnCall {
	w.suite.Helper()
	if calls == nil {
		w.suite.Fatalf(afterAllNilCallError)
		return nil
	}

	for _, currCall := range calls.calls() {
		w.iMockCall.After(currCall, offsetInterval...)
	}
	return w
}
func (w *mockFnCall) Waiting(timeout time.Duration) MockFnCall {
	w.iMockCall.Waiting(timeout)
	return w
}
func (w *mockFnCall) Bind(token *MockedCall) MockedFn {
	w.suite.Helper()
	if token == nil {
		w.suite.Fatalf(bindNilMockedCallError)
		return nil
	}

	*token = w
	return w
}
func (w *mockFnCall) Repeat(times uint) MockFnCalls {
	w.suite.Helper()
	if times < 2 {
		w.mockFn.suite.Fatalf(repeatNotPluralError)
		return nil
	}

	for i := uint(0); i < times-1; i++ {
		w.mockFn.syncCalls = append(w.mockFn.syncCalls, &mockedCall{w.Copy()})
	}
	return w.mockFn
}
func (w *mockFnCall) calls() []MockedCall {
	return []MockedCall{w}
}

// -----

type MockFnCallArgs interface {
	Returning(functionResults ...interface{}) MockFnCallArgs
	Before(call MockedCall, offsetInterval ...time.Duration) MockFnCallArgs
	BeforeAll(call MockedCalls, offsetInterval ...time.Duration) MockFnCallArgs
	After(call MockedCall, offsetInterval ...time.Duration) MockFnCallArgs
	AfterAll(call MockedCalls, offsetInterval ...time.Duration) MockFnCallArgs
	Waiting(timeout time.Duration) MockFnCallArgs
	Bind(token *MockedCall) MockedFn
	Repeat(times uint) MockFnCalls
	MockedFn
}
type mockFnCallArgs struct {
	*mockFnCall
}

func (w *mockFnCallArgs) Returning(functionResults ...interface{}) MockFnCallArgs {
	w.mockFnCall.Returning(functionResults...)
	return w
}
func (w *mockFnCallArgs) Before(call MockedCall, offsetInterval ...time.Duration) MockFnCallArgs {
	w.suite.Helper()
	w.mockFnCall.Before(call, offsetInterval...)
	return w
}
func (w *mockFnCallArgs) BeforeAll(calls MockedCalls, offsetInterval ...time.Duration) MockFnCallArgs {
	w.suite.Helper()
	w.mockFnCall.BeforeAll(calls, offsetInterval...)
	return w
}
func (w *mockFnCallArgs) After(call MockedCall, offsetInterval ...time.Duration) MockFnCallArgs {
	w.suite.Helper()
	w.mockFnCall.After(call, offsetInterval...)
	return w
}
func (w *mockFnCallArgs) AfterAll(calls MockedCalls, offsetInterval ...time.Duration) MockFnCallArgs {
	w.suite.Helper()
	w.mockFnCall.AfterAll(calls, offsetInterval...)
	return w
}
func (w *mockFnCallArgs) Waiting(timeout time.Duration) MockFnCallArgs {
	w.mockFnCall.Waiting(timeout)
	return w
}

// -----

const beforeNilCallError = `Please, give a non-nil MockedCall to the 'Before' method (Check the order of the 'Bind' and 'Before' method calls)`
const beforeAllNilCallError = `Please, give a non-nil MockedCall to the 'BeforeAll' method (Check the order of the 'Bind' and 'BeforeAll' method calls)`
const afterNilCallError = `Please, give a non-nil MockedCall to the 'After' method (Check the order of the 'Bind' and 'After' method calls)`
const afterAllNilCallError = `Please, give a non-nil MockedCall to the 'AfterAll' method (Check the order of the 'Bind' and 'AfterAll' method calls)`
const repeatNotPluralError = "You can only call the Repeat method when the 'times' argument is lesser than 2"
