module gitlab.com/loxe-tools/go-test

go 1.14

require (
	gitlab.com/loxe-tools/go-base-library v0.0.29
	gitlab.com/loxe-tools/go-validation v1.0.41
	golang.org/x/tools v0.0.0-20200929223013-bf155c11ec6f
)
