package graphql

func a() {
	gql := suite.NewGraphQL("http://address.graphql.endpoint")

	gql.QueryᐸaccountByIDᐳ(id).
		AskForᐸAccountᐳ(FieldsᐸAccountᐳ.
			Nickname().
			User(FieldsᐸAccountᐳ.
				Name().
				Email(),
			).
			Role(companyName).
			Posts(FieldsᐸAccountᐳ.
				Title().
				Date(),
			),
		)
	gql.QueryᐸaccountByIDᐳ(id).
		AskForᐸAccountᐳ(FieldsᐸAccountᐳ{
			Nickname: true,
			User: FieldsᐸUserᐳ{
				Name: true,
				Email: true,
			},
			Role: companyName,
			Posts: FieldsᐸPostsᐳ{
				Title: true,
				Date: true,
			},
		})
	gql.QueryᐸaccountByIDᐳ(id).
		AskForᐸAccountᐳ(
			Account.Nickname,
			Account.User(
				User.Name,
				User.Email,
			),
			Account.Role(companyName),
			Account.Posts(postsQt,
				Post.Title,
				Post.Date,
			),
		)
	gql.QueryᐸaccountByIDᐳ(id).
		AskForᐸAccountᐳ(func(fieldSelector FieldsᐸAccountᐳ) FieldsᐸAccountᐳ {
			return fieldSelector.
				Nickname().
				User(func(fieldSelector FieldsᐸUserᐳ) FieldsᐸUserᐳ {
					return fieldSelector.
						Name()
				}).
				Role(companyName).
				Posts(postsQt, func(fieldSelector FieldsᐸPostᐳ) FieldsᐸPostᐳ {
					return fieldSelector.
						Date().
						Title()
				})
		})
}
