package mockCall

import "time"

func (m *MockCall) checkPreviousCalls(thisCall *call) {
	for _, currMockedCall := range *m.previousCalls {
		prevCall := currMockedCall._getCall()
		if prevCall == nil {
			m.suite.Fatalf(previousCallNotCalledError, m.FileLine(), currMockedCall.FileLine())
			return
		}

		if prevCall.timestamp.After(thisCall.timestamp) {
			m.suite.Fatalf(previousCallHappenedAfterError,
				m.FileLine(),
				currMockedCall.FileLine(),
				prevCall.timestamp.Sub(thisCall.timestamp))
			return
		}

		if currMockedCall.minOffset != 0 {
			minOffset := thisCall.timestamp.Add(-currMockedCall.minOffset)
			if prevCall.timestamp.After(minOffset) {
				m.suite.Fatalf(previousCallHappenedAfterMinOffsetError,
					m.FileLine(),
					currMockedCall.minOffset,
					currMockedCall.FileLine(),
					thisCall.timestamp.Sub(prevCall.timestamp))
				return
			}
		}

		if currMockedCall.maxOffset == -1 {
			continue
		}

		maxOffset := thisCall.timestamp.Add(-currMockedCall.maxOffset)
		if prevCall.timestamp.Before(maxOffset) {
			m.suite.Fatalf(previousCallHappenedBeforeMaxOffsetError,
				m.FileLine(),
				currMockedCall.maxOffset,
				currMockedCall.FileLine(),
				thisCall.timestamp.Sub(prevCall.timestamp))
			return
		}
	}
}
func (m *MockCall) newOrderedCall(call MockedCall, delay ...time.Duration) *orderedCall {
	m.suite.Helper()
	newCall := &orderedCall{
		call,
		0,
		-1,
	}
	if len(delay) > 0 {
		newCall.minOffset = delay[0]
		if len(delay) > 1 {
			newCall.maxOffset = delay[1]
			if newCall.maxOffset <= newCall.minOffset {
				m.suite.Fatalf(offsetIntervalValuesOverlapError)
			}
		}
	}

	return newCall
}

// -----

type orderedCall struct {
	MockedCall
	minOffset  time.Duration
	maxOffset  time.Duration
}

const previousCallNotCalledError = `The following mocked function call:
	%s
was expected to be called AFTER this other mocked function call:
	%s
but it was called before even the other call happened`

const previousCallHappenedAfterError = `The following mocked function call:
	%s
was expected to be called AFTER this other mocked function call:
	%s
but it was called %s BEFORE`

const previousCallHappenedAfterMinOffsetError = `The following mocked function call:
	%s
was expected to be called at least %s AFTER this other mocked function call:
	%s
but it was called only %v later`

const previousCallHappenedBeforeMaxOffsetError = `The following mocked function call:
	%s
was expected to be called at most %s AFTER this other mocked function call:
	%s
but it was called only after %v`

const offsetIntervalValuesOverlapError = "the Before/After method param min/max offset cannot overlap"