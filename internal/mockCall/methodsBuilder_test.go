package mockCall

import (
	"gitlab.com/loxe-tools/go-test/internal"
	"reflect"
	"testing"
	"time"
)

func TestMethodsBuilder(t *testing.T) {
	suite := &internal.TestSuite{t}

	suite.Run("When the 'IgnoringArgs' is called, set the 'ignoreArgs' boolean to true", func(suite *internal.TestSuite) {
		mock := New(suite)
		mock.IgnoringArgs()
		if !mock.ignoreArgs {
			suite.Fatalf("The 'ignoreArgs' field must be set to true after calling the 'IgnoringArgs' method")
		}
	})
	suite.Run("When the 'Expecting' method is called, the 'arguments' field must be set to the given arguments", func(suite *internal.TestSuite) {
		args := []interface{}{1, "any", false, "string"}
		mock := New(suite)

		mock.Expecting(args...)
		if !reflect.DeepEqual(mock.arguments, args) {
			suite.Fatalf("The method 'Expecting' didn't set the 'arguments' field properly")
		}
	})
	suite.Run("When the 'AsStub' method is called, the 'stub' field must be set to true and the 'results' field set to the given arguments", func(suite *internal.TestSuite) {
		results := []interface{}{1, "any", false, "string"}
		mock := New(suite)

		mock.AsStub(results...)
		if !mock.stub {
			suite.Fatalf("The method 'AsStub' didn't set the 'stub' field to true")
		}
		if !reflect.DeepEqual(mock.results, results) {
			suite.Fatalf("The method 'AsStub' didn't set the 'results' field properly")
		}
	})
	suite.Run("When the 'Returning' method is called, the 'results' field must be set to the given arguments", func(suite *internal.TestSuite) {
		results := []interface{}{1, "any", false, "string"}
		mock := New(suite)

		mock.Returning(results...)
		if !reflect.DeepEqual(mock.results, results) {
			suite.Fatalf("The method 'Returning' didn't set the 'results' field properly")
		}
	})
	suite.Run("When the 'UsingComparators' method is called, the 'comparators' field must be set", func(suite *internal.TestSuite) {
		mock := New(suite)
		called := false

		mock.UsingComparators(func(a, b interface{}) bool {
			called = true
			return true
		})
		if len(mock.comparators) != 1 {
			suite.Fatalf("The method 'UsingComparators' didn't set the 'comparators' field properly")
		}
		mock.comparators[0](nil, nil)
		if !called {
			suite.Fatalf("The method 'UsingComparators' didn't set the 'comparators' field properly")
		}
	})
	suite.Run("When the 'Before' method is called:", func(suite *internal.TestSuite) {
		suite.Run("Should call the suite 'Helper' method", func(suite *internal.TestSuite) {
			helperCalls := 0
			mockSuite := &internal.MockSuite{Helper_: func() {
				helperCalls += 1
			}}
			mock := New(mockSuite)
			call := New(suite)

			mock.Before(call)
			if helperCalls != 1 {
				suite.Fatalf("The 'Before' method didn't called the suite 'Helper' method correctly")
			}
		})
		suite.Run("The 'previousCalls' of the GIVEN CALL (not the method receiver) should be set", func(suite *internal.TestSuite) {
			mock := New(suite)
			call := New(suite)

			mock.Before(call)
			if len(*call.previousCalls) != 1 || (*call.previousCalls)[0].MockedCall.(*MockCall) != mock {
				suite.Fatalf("The method 'Before' didn't set the 'previousCalls' field properly")
			}
		})
		suite.Run("Must append a reference to the given call as a lambda function", func(suite *internal.TestSuite) {
			suite.Run("When there's no other stored call, just set the 'nextCalls' to a singleton with the given call", func(suite *internal.TestSuite) {
				mock := New(suite)
				call := New(suite)

				mock.Before(call)
				if len(mock.nextCalls) != 1 {
					suite.Fatalf("The method 'Before' didn't set the 'nextCalls' field properly")
				}
			})
			suite.Run("When there's other stored calls, append the given call to the 'nextCalls' field, preserving the old ones", func(suite *internal.TestSuite) {
				mock := New(suite)
				call := New(suite)
				old := New(suite)
				mock.Before(old)

				mock.Before(call)
				if len(mock.nextCalls) != 2 {
					suite.Fatalf("The method 'Before' didn't set the 'nextCalls' field properly")
				}
			})
			suite.Run("When the lambda is called, the MockedCall given to the 'Before' method must call it's '_after' method, with an arbitrary third MockedCall and the 'Before' offset interval", func(suite *internal.TestSuite) {
				mock := New(suite)
				call := New(suite)
				thirdyCall := New(suite)
				minOffset, maxOffset := time.Millisecond, time.Second

				mock.Before(call, minOffset, maxOffset)
				mock.nextCalls[0](thirdyCall)
				if len(*call.previousCalls) != 2 {
					suite.Fatalf("The method 'Before' didn't saved a correct lambda function to the given call")
				}
				prevCall := (*call.previousCalls)[1]
				if prevCall.MockedCall.(*MockCall) != thirdyCall {
					suite.Fatalf("The method 'Before' didn't saved a correct lambda function to the given call")
				}
				if prevCall.minOffset != minOffset || prevCall.maxOffset != maxOffset {
					suite.Fatalf("The method 'Before' didn't saved a correct lambda function to the given call")
				}
			})
		})
		suite.Run("Should use the offset interval, if set", func(suite *internal.TestSuite) {
			mock := New(suite)
			call := New(suite)
			minOffset, maxOffset := time.Millisecond, time.Second

			mock.Before(call, minOffset, maxOffset)
			if (*call.previousCalls)[0].minOffset != minOffset || (*call.previousCalls)[0].maxOffset != maxOffset {
				suite.Fatalf("The method 'Before' didn't set the offset interval of the previous call properly")
			}
		})
	})
	suite.Run("When the 'After' method is called:", func(suite *internal.TestSuite) {
		suite.Run("Should call the suite 'Helper' method", func(suite *internal.TestSuite) {
			helperCalls := 0
			mockSuite := &internal.MockSuite{Helper_: func() {
				helperCalls += 1
			}}
			mock := New(mockSuite)
			call := New(suite)

			mock.After(call)
			if helperCalls != 3 { // consider that the method '_after' and 'newOrderedCall' call the 'Helper' too
				suite.Fatalf("The 'After' method didn't called the suite 'Helper' method correctly")
			}
		})
		suite.Run("The given MockedCall should be appended to the 'previousCalls' slice", func(suite *internal.TestSuite) {
			mock := New(suite)
			call := New(suite)

			mock.After(call)
			if len(*mock.previousCalls) != 1 || (*mock.previousCalls)[0].MockedCall.(*MockCall) != call {
				suite.Fatalf("The method 'After' didn't set the 'previousCalls' field properly")
			}
		})
		suite.Run("Should use the offset interval, if set", func(suite *internal.TestSuite) {
			mock := New(suite)
			call := New(suite)
			minOffset, maxOffset := time.Millisecond, time.Second

			mock.After(call, minOffset, maxOffset)
			if (*mock.previousCalls)[0].minOffset != minOffset || (*mock.previousCalls)[0].maxOffset != maxOffset {
				suite.Fatalf("The method 'After' didn't set the offset interval of the previous call properly")
			}
		})
	})
	suite.Run("When the 'Waiting' method is called, the 'timeout' field must be set", func(suite *internal.TestSuite) {
		mock := New(suite)
		timeout := time.Second

		mock.Waiting(timeout)
		if mock.timeout != timeout {
			suite.Fatalf("The method 'Waiting' didn't set the 'timeout' field properly")
		}
	})
}
