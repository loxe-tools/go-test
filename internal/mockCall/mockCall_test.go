package mockCall

import (
	"gitlab.com/loxe-tools/go-test/internal"
	"testing"
)

func TestNew(t *testing.T) {
	suite := &internal.TestSuite{t}

	suite.Run("Don't return a nil pointer", func(suite *internal.TestSuite) {
		if New(suite) == nil {
			suite.Fatalf("The 'New' function returned a nil pointer")
		}
	})
	suite.Run("Should correctly set the 'suite' field", func(suite *internal.TestSuite) {
		mock := New(suite)
		if mock.suite == nil || mock.suite.(*internal.TestSuite) != suite {
			suite.Fatalf("The 'New' function didn't created a *MockCall with the correct 'suite' field")
		}
	})
	suite.Run("Without 'skipFrames' argument, should correctly set the 'fileLine' field", func(suite *internal.TestSuite) {
		mock, fileLine := New(suite), internal.FnCallFileLine(0)
		if mock.fileLine != fileLine {
			suite.Fatalf("The 'New' function didn't created a *MockCall with the correct 'fileLine' field")
		}
	})
	suite.Run("Should correctly set the 'fileLine' field, considering the 'skipFrames' argument", func(suite *internal.TestSuite) {
		mock, fileLine := New(suite, 1), internal.FnCallFileLine(1)
		if mock.fileLine != fileLine {
			suite.Fatalf("The 'New' function didn't created a *MockCall with the correct 'fileLine' field (with 'skipFrames')")
		}
	})
	suite.Run("Should set the 'stub' field to false", func(suite *internal.TestSuite) {
		mock := New(suite)
		if mock.stub {
			suite.Fatalf("The 'New' function didn't created a *MockCall with the 'stub' field set to false")
		}
	})
	suite.Run("Should set the 'ignoreArgs' field to false", func(suite *internal.TestSuite) {
		mock := New(suite)
		if mock.ignoreArgs {
			suite.Fatalf("The 'New' function didn't created a *MockCall with the 'ignoreArgs' field set to false")
		}
	})
	suite.Run("Should set the 'timeout' field to zero", func(suite *internal.TestSuite) {
		mock := New(suite)
		if mock.timeout != 0 {
			suite.Fatalf("The 'New' function didn't created a *MockCall with the 'timeout' field set to zero")
		}
	})
	suite.Run("Should set the 'arguments' field set to nil", func(suite *internal.TestSuite) {
		mock := New(suite)
		if mock.arguments != nil {
			suite.Fatalf("The 'New' function didn't created a *MockCall with the 'arguments' field set to nil")
		}
	})
	suite.Run("Should set the 'results' field set to nil", func(suite *internal.TestSuite) {
		mock := New(suite)
		if mock.results != nil {
			suite.Fatalf("The 'New' function didn't created a *MockCall with the 'results' field set to nil")
		}
	})
	suite.Run("Should set the 'comparators' field set to nil", func(suite *internal.TestSuite) {
		mock := New(suite)
		if mock.comparators != nil {
			suite.Fatalf("The 'New' function didn't created a *MockCall with the 'comparators' field set to nil")
		}
	})
	suite.Run("Should set the 'nextCalls' field set to nil", func(suite *internal.TestSuite) {
		mock := New(suite)
		if mock.nextCalls != nil {
			suite.Fatalf("The 'New' function didn't created a *MockCall with the 'nextCalls' field set to nil")
		}
	})
	suite.Run("Should set the 'previousCalls' pointer to slice field set to an empty slice", func(suite *internal.TestSuite) {
		mock := New(suite)
		if mock.previousCalls == nil || len(*mock.previousCalls) != 0 {
			suite.Fatalf("The 'New' function didn't created a *MockCall with the 'previousCalls' field set to an empty slice")
		}
	})
	suite.Run("Should set the 'channel' field to an empty channel with cap == 1", func(suite *internal.TestSuite) {
		mock := New(suite)
		if mock.channel == nil || len(mock.channel) != 0 || cap(mock.channel) != 1 {
			suite.Fatalf("The 'New' function didn't created a *MockCall with the 'channel' field to an empty channel with cap == 1")
		}
	})
	suite.Run("MockCalls are created with nil values for the 'call' field", func(suite *internal.TestSuite) {
		mock := New(suite)
		if mock.call != nil {
			suite.Fatalf("The 'New' function didn't created a *MockCall with the 'call' field set to nil")
		}
	})
}
