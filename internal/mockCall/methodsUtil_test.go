package mockCall

import (
	"gitlab.com/loxe-tools/go-test/internal"
	"reflect"
	"testing"
	"time"
)

func TestMethodsUtil(t *testing.T) {
	suite := &internal.TestSuite{t}

	suite.Run("The 'ExpectedArgs' method should return the expected arguments for the current call", func(suite *internal.TestSuite) {
		expectedArgs := []interface{}{1, "anything"}
		mock := New(suite)
		mock.Expecting(expectedArgs...)

		if !reflect.DeepEqual(mock.ExpectedArgs(), expectedArgs) {
			suite.Fatalf("The 'ExpectedArgs' method didn't returned the correct expected arguments")
		}
	})
	suite.Run("The 'Called' method should return TRUE only if the current call has been called", func(suite *internal.TestSuite) {
		mock := New(suite)
		mock.Call()

		if !mock.Called() {
			suite.Fatalf("The 'Called' method returned FALSE for a call that has been called")
		}
	})
	suite.Run("Wait", func(suite *internal.TestSuite) {
		suite.Run("If the call already happened, just return immediately (even with a timeout) and don't throw failure", func(suite *internal.TestSuite) {
			fatalfCalls := 0
			mockSuite := &internal.MockSuite{Fatalf_: func(msg string, args ...interface{}) {
				fatalfCalls += 1
			}}
			mock := New(mockSuite)
			mock.Waiting(time.Millisecond * 100)

			// setting directly instead of calling 'Call' method to avoid sending a value to the chan,
			// thus forcing the select to timeout
			mock.call = &call{}

			mock.Wait()
			if fatalfCalls != 0 {
				suite.Fatalf("The 'Wait' method threw a failure for an already called mock")
			}
		})
		suite.Run("If the timeout isn't set and the call didn't happened, fail the test", func(suite *internal.TestSuite) {
			fileLine := "custom fileLine"
			fatalfCalls := 0
			mockSuite := &internal.MockSuite{Fatalf_: func(msg string, args ...interface{}) {
				fatalfCalls += 1
				if msg != notCalledError {
					suite.Fatalf("The 'Wait' method didn't used the correct error message to mocks that are not called (without timeout)")
				}
				if len(args) != 1 || args[0].(string) != fileLine {
					suite.Fatalf("The 'Wait' method didn't used the correct arguments to the error message")
				}
			}}
			mock := New(mockSuite)
			mock.fileLine = fileLine

			mock.Wait()
			if fatalfCalls != 1 {
				suite.Fatalf("The 'Wait' method didn't threw a failure for a mock that hasn't been called")
			}
		})
		suite.Run("If the timeout is set and the call don't happen until it, throw a failure", func(suite *internal.TestSuite) {
			fileLine := "custom fileLine"
			timeout := time.Millisecond * 100
			fatalfCalls := 0
			mockSuite := &internal.MockSuite{Fatalf_: func(msg string, args ...interface{}) {
				fatalfCalls += 1
				if msg != timeoutError {
					suite.Fatalf("The 'Wait' method didn't used the correct error message to mocks that are not called within a timeout")
				}
				if len(args) != 2 || args[0].(string) != fileLine || args[1].(time.Duration) != timeout {
					suite.Fatalf("The 'Wait' method didn't used the correct arguments to the error message")
				}
			}}
			mock := New(mockSuite)
			mock.fileLine = fileLine
			mock.Waiting(timeout)

			mock.Wait()
			if fatalfCalls != 1 {
				suite.Fatalf("The 'Wait' method didn't threw a failure for a mock that hasn't been called within a timeout")
			}
		})
		suite.Run("If the timeout is set and the call happens before the time expires, dont throw a failure", func(suite *internal.TestSuite) {
			fatalfCalls := 0
			mockSuite := &internal.MockSuite{Fatalf_: func(msg string, args ...interface{}) {
				fatalfCalls += 1
			}}
			mock := New(mockSuite)
			mock.Waiting(time.Millisecond * 100)

			go func() {
				time.Sleep(time.Millisecond * 50)
				mock.Call()
			}()
			mock.Wait()
			if fatalfCalls != 0 {
				suite.Fatalf("The 'Wait' method threw a failure for a called mock")
			}
		})
	})
	suite.Run("MatchArgs", func(suite *internal.TestSuite) {
		suite.Run("If the current mock is a stub, always return true", func(suite *internal.TestSuite) {
			mock := New(suite)
			mock.AsStub()
			if !mock.MatchArgs(1, "any", "arguments") {
				suite.Fatalf("When the mock is a stub, 'MatchArgs' must always return true")
			}
		})
		suite.Run("When ignoring arguments, the MatchArgs should always return true", func(suite *internal.TestSuite) {
			mock := New(suite)
			mock.IgnoringArgs()
			if !mock.MatchArgs(1, "any", "arguments") {
				suite.Fatalf("When 'IgnoringArgs' was previously called, 'MatchArgs' must always return true")
			}
		})
		suite.Run("If the length of the received arguments and expected arguments slices are different, return false (dont even compare)", func(suite *internal.TestSuite) {
			mock := New(suite)
			mock.Expecting(1, "any")

			if mock.MatchArgs(1) {
				suite.Fatalf("'MatchArgs' returned true with the size of the received arguments slice different from the expected")
			}
		})
		suite.Run("Return false if at least one of the arguments don't match (comparing received[i] with expected[i])", func(suite *internal.TestSuite) {
			mock := New(suite)
			mock.Expecting(1, "any", true)

			if mock.MatchArgs(1, "any", false) {
				suite.Fatalf("'MatchArgs' returned true with the received arguments containing at least one argument different from the expected")
			}
		})
		suite.Run("If there are custom comparators, use them to do the comparison", func(suite *internal.TestSuite) {
			mock := New(suite)
			mock.Expecting(true, 10, "value")

			// type assert to ensure that the custom comparators are used against
			// the argument of same index
			mock.UsingComparators(
				func(expected, received interface{}) bool {
					_, isBool := received.(bool)
					return isBool
				},
				func(expected, received interface{}) bool {
					_, isInt := received.(int)
					return isInt
				},
				func(expected, received interface{}) bool {
					_, isString := received.(string)
					return isString
				},
			)

			if !mock.MatchArgs(false, 54, "different value") {
				suite.Fatalf("'MatchArgs' returned false when using a custom comparator that always return true")
			}
		})
		suite.Run("If there are custom comparators, and one of them is nil, fallback to the default comparator", func(suite *internal.TestSuite) {
			mock := New(suite)
			mock.Expecting(true, "defaultComparator", "any")
			mock.UsingComparators(
				func(expected, received interface{}) bool { return true },
				nil,
				func(expected, received interface{}) bool { return true },
			)

			if mock.MatchArgs(false, "Should throw Failure", "any, but different") {
				suite.Fatalf("'MatchArgs' didn't fallback to the default comparator and returned true for a different argument")
			}
		})
		suite.Run("If all the arguments are equal, without custom comparators, return true", func(suite *internal.TestSuite) {
			mock := New(suite)
			mock.Expecting(1, "any", true)

			if !mock.MatchArgs(1, "any", true) {
				suite.Fatalf("'MatchArgs' returned false with the received arguments being equal to the expected")
			}
		})
		suite.Run("If all the arguments are equal, using any custom comparators, return true", func(suite *internal.TestSuite) {
			mock := New(suite)
			mock.Expecting(1, "value", true)
			mock.UsingComparators(
				nil,
				func(expected, received interface{}) bool { return true },
				nil,
			)

			if !mock.MatchArgs(1, "different value", true) {
				suite.Fatalf("'MatchArgs' returned false with the received arguments being equal to the expected")
			}
		})
	})
	suite.Run("Call", func(suite *internal.TestSuite) {
		suite.Run("Ensure that the 'Call' method calls the 'checkPreviousCalls' method", func(suite *internal.TestSuite) {
			fileLine := "custom fileLine"
			prevFileLine := "custom fileLine 2"
			fatalfCalls := 0
			mockSuite := &internal.MockSuite{
				Fatalf_: func(msg string, args ...interface{}) {
					fatalfCalls += 1
					if msg != previousCallNotCalledError {
						suite.Fatalf("The 'Call' didn't threw the correct failure message when the given previous call didn't had been called")
					}
					if len(args) != 2 || args[0].(string) != fileLine || args[1].(string) != prevFileLine {
						suite.Fatalf("The 'Call' didn't used the correct error message arguments when the given previous call didn't had been called")
					}
				},
				Helper_: func() {},
			}
			mock := New(mockSuite)
			mock.fileLine = fileLine
			prev := New(suite)
			prev.fileLine = prevFileLine

			mock.After(prev)
			mock.Call()
			if fatalfCalls != 1 {
				suite.Fatalf("The 'Call' didn't threw failure when the given previous call didn't had been called")
			}
		})
		suite.Run("Must send a value to the channel", func(suite *internal.TestSuite) {
			mock := New(suite)

			mock.Call()
			if len(mock.channel) != 1 {
				suite.Fatalf("The 'Call' method didn't sent a value to the channel after being called")
			}
		})
		suite.Run("Should fill the 'call' field with the current arguments and a timestamp", func(suite *internal.TestSuite) {
			args := []interface{}{1, "any"}
			mock := New(suite)

			mock.Call(args...)
			if mock.call == nil {
				suite.Fatalf("The 'Call' method didn't set the 'call' field")
			}
			if mock.call.timestamp.IsZero() {
				suite.Fatalf("The 'Call' method didn't set valid 'timestamp' field value inside the 'call' field")
			}
			if !reflect.DeepEqual(mock.call.arguments, args) {
				suite.Fatalf("The 'Call' method didn't set the 'arguments' field value of the 'call' field correctly")
			}
		})
		suite.Run("Should return the 'results' field value", func(suite *internal.TestSuite) {
			results := []interface{}{"any", 1}
			mock := New(suite)
			mock.results = results

			receivedResults := mock.Call()
			if !reflect.DeepEqual(receivedResults, results) {
				suite.Fatalf("The 'Call' method didn't returned the correct values")
			}
		})
	})
	suite.Run("Copy", func(suite *internal.TestSuite) {
		suite.Run("'Immutable' fields", func(suite *internal.TestSuite) {
			nextCalls_Calls := 0
			mock := &MockCall{
				suite:       suite,
				fileLine:    "fileLine",
				stub:        true,
				ignoreArgs:  true,
				timeout:     time.Second * 3,
				arguments:   []interface{}{"any"},
				results:     []interface{}{1, 2},
				comparators: []func(a, b interface{}) bool{nil},
				nextCalls:   []func(MockedCall){func(mockedCall MockedCall) { nextCalls_Calls += 1 }},

				// Fields that can mutate (just ignore in this test)
				previousCalls: nil,
				channel:       nil,
				call:          nil,
			}

			copied := mock.Copy()
			if mock.suite != copied.suite ||
				mock.fileLine != copied.fileLine ||
				mock.stub != copied.stub ||
				mock.ignoreArgs != copied.ignoreArgs ||
				mock.timeout != copied.timeout ||
				!reflect.DeepEqual(mock.arguments, copied.arguments) ||
				!reflect.DeepEqual(mock.results, copied.results) ||
				!reflect.DeepEqual(mock.comparators, copied.comparators) ||
				len(mock.nextCalls) != len(copied.nextCalls) {
				suite.Fatalf("The 'Copy' method didn't copied the immutable fields properly")
			}

			// ensure that the copied 'nexCalls' slice is the same
			aux := nextCalls_Calls
			copied.nextCalls[0](nil)
			if aux >= nextCalls_Calls {
				suite.Fatalf("The 'Copy' method didn't copied the immutable fields properly")
			}
		})
		suite.Run("'Mutable' fields", func(suite *internal.TestSuite) {
			mock := &MockCall{
				// Immutable fields (just ignore in this test)
				suite:       nil,
				fileLine:    "",
				stub:        false,
				ignoreArgs:  false,
				timeout:     0,
				arguments:   nil,
				results:     nil,
				comparators: nil,
				nextCalls:   nil,

				// Arbitrary values
				previousCalls: &[]*orderedCall{{minOffset: 10}},
				channel:       make(chan byte, 9),
				call:          &call{time.Now(), nil},
			}

			copied := mock.Copy()
			if copied.previousCalls != mock.previousCalls ||
				copied.channel == mock.channel ||
				cap(copied.channel) != 1 ||
				copied.call != nil {
				suite.Fatalf("The 'Copy' method didn't copied the mutable fields properly")
			}
		})
		suite.Run("Call all the lambda function in the 'nextCalls' field slice", func(suite *internal.TestSuite) {
			a, b, c := 0, 0, 0
			mock := New(suite)
			mock.nextCalls = []func(MockedCall){
				func(MockedCall) { a += 1 },
				func(MockedCall) { b += 1 },
				func(MockedCall) { c += 1 },
			}

			mock.Copy()
			if a != 1 || b != 1 || c != 1 {
				suite.Fatalf("The 'Copy' method didn't called the 'nextCalls' lambda functions")
			}
		})
	})
}
