package mockCall

import "time"

// MockedCall is an interface that is built in a way
// that is impossible (without workarounds) to satisfy
// outside this package
type MockedCall interface {
	_after(call MockedCall, offsetInterval ...time.Duration)
	_getCall() *call
	FileLine() string // Exported just to easy tests (doesn't affects encapsulation)
}

func (m *MockCall) _after(call MockedCall, offsetInterval ...time.Duration) {
	m.suite.Helper()
	*m.previousCalls = append(*m.previousCalls, m.newOrderedCall(call, offsetInterval...))
}
func (m *MockCall) _getCall() *call {
	return m.call
}

// Exported just to easy tests (doesn't affects encapsulation)
func (m *MockCall) FileLine() string {
	return m.fileLine
}

//-----

type call struct {
	timestamp time.Time
	arguments []interface{}
}
