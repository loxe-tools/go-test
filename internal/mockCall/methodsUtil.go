package mockCall

import (
	"reflect"
	"time"
)

func (m *MockCall) ExpectedArgs() []interface{} {
	return m.arguments
}
func (m *MockCall) Called() bool {
	return m.call != nil
}
func (m *MockCall) Wait() {
	if m.call != nil {
		return
	}
	if m.timeout == 0 && m.call == nil {
		m.suite.Fatalf(notCalledError, m.FileLine())
		return
	}

	select {
	case <-m.channel:
		return
	case <-time.After(m.timeout):
		m.suite.Fatalf(timeoutError, m.FileLine(), m.timeout)
	}
}
func (m *MockCall) MatchArgs(receivedArgs ...interface{}) bool {
	if m.stub || m.ignoreArgs {
		return true
	}

	if len(receivedArgs) != len(m.arguments) {
		return false
	}

	for i := 0; i < len(m.arguments); i++ {
		a, b := m.arguments[i], receivedArgs[i]

		argIsEqual := defaultComparator
		if len(m.comparators) >= i+1 && m.comparators[i] != nil {
			argIsEqual = m.comparators[i]
		}

		if !argIsEqual(a, b) {
			return false
		}
	}

	return true
}
func (m *MockCall) Call(receivedArgs ...interface{}) []interface{} {
	thisCall := &call{
		timestamp: time.Now(),
		arguments: receivedArgs,
	}
	m.checkPreviousCalls(thisCall)

	m.call = thisCall
	m.channel <- 0
	return m.results
}
func (m *MockCall) Copy() *MockCall {
	newMockCall := &MockCall{
		// Don't expect these fields to change after the first definition
		m.suite,
		m.fileLine,
		m.stub,
		m.ignoreArgs,
		m.timeout,
		m.arguments,
		m.results,
		m.comparators,
		m.nextCalls,

		m.previousCalls, // Since its a pointer to slice, its safe to just copy the pointer
		make(chan byte, 1),
		nil,
	}

	// Since there's only the 'after' method implementation, when calls are copied
	// is necessary to signal to all the other calls that this new copied mock needs
	// to be added as a 'previous' call too
	for _, currOtherMock := range m.nextCalls {
		currOtherMock(newMockCall)
	}

	return newMockCall
}

// -----

var defaultComparator = reflect.DeepEqual

const notCalledError = `The following mock function call not happened:
	%s`

const timeoutError = `The following mock function call not happened:
	%s
timeout: %v`
