package generate

import (
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goParser"
	"gitlab.com/loxe-tools/go-test/internal/generate/input"
)

func resolveFocus(arguments input.Arguments) *goParser.ParserFocus {
	fileFocus := goParser.FocusFilePath(arguments.SourceFile)
	if arguments.SourceType != "" {
		return goParser.FocusMerge(fileFocus, goParser.FocusTypeName(arguments.SourceType))
	}
	return fileFocus
}
