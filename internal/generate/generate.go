package generate

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goParser"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"gitlab.com/loxe-tools/go-test/internal"
	"gitlab.com/loxe-tools/go-test/internal/generate/code"
	"gitlab.com/loxe-tools/go-test/internal/generate/input"
	"golang.org/x/tools/go/packages"
	"path/filepath"
	"strings"
)

func Generate(arguments input.Arguments) {
	log := logCLI.NewLogCLI(arguments.Debug, arguments.SupportsANSI)
	arguments = handleParams(arguments, log)

	parserLog := log.Info("Parsing source package to filter not eligible types")
	config := goParser.Config{
		Dir:   "./",
		Focus: resolveFocus(arguments),
	}
	pattern := fmt.Sprintf("file=%s", arguments.SourceFile)
	parser, e := goParser.NewGoParser(pattern, config, *parserLog)
	if e != nil {
		log.Fatal("%v", e)
	}

	pkgs, _ := packages.Load(&packages.Config{Mode: packages.NeedName}, pattern)
	file, e := code.Generate(parser, filepath.Base(strings.TrimSuffix(arguments.SourceFile, ".go")), pkgs[0].Name, pkgs[0].PkgPath)
	if e != nil {
		log.Fatal("%v", e)
	}

	e = file.Save(
		fmt.Sprintf("%s %s", internal.LibraryModulePath, internal.LibraryModuleVersion),
		filepath.Dir(arguments.SourceFile))
	if e != nil {
		log.Fatal("%v", e)
	}
	log.Info("The end!")
}
