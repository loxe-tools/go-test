package generate

import (
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goParser/helpers"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"gitlab.com/loxe-tools/go-test/internal/generate/code/templates"
	"gitlab.com/loxe-tools/go-test/internal/generate/input"
)

func handleParams(arguments input.Arguments, log *logCLI.LogCLI) input.Arguments {
	input.Validate(arguments, log.Debug("Validating arguments..."))

	normalizeLog := log.Debug("Normalizing arguments...")
	arguments = input.Normalize(arguments, normalizeLog)
	input.PrintArguments(arguments, normalizeLog)

	if arguments.OnlyAsciiTypenames {
		e := templates.SetIdentifierToTypeName(helpers.IdentifierToAsciiTypeName)
		if e != nil {
			log.Fatal("%v", e)
		}
	}

	return arguments
}
