package code

import (
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goFile/goImports"
	"gitlab.com/loxe-tools/go-test/internal"
)

const mockFnPackagePath = internal.LibraryModulePath + "/mockFn"
const mockFnPackageName = "mockFn"

func baseImports(packageImportPath string) *goImports.GoImports {
	i := goImports.NewGoImports(packageImportPath)
	i.AddImport(mockFnPackageName, mockFnPackagePath)
	i.AddImport("time", "time")
	return i
}
