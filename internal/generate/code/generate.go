package code

import (
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goFile"
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goParser"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"go/types"
)

func Generate(parser *goParser.GoParser, fileName, packageName, packagePath string) (*goFile.GoFile, error) {
	fileImports := baseImports(packagePath)
	builtTypes := map[string]bool{}

	accumulatedCode := generatedCode{"", "", ""}
	e := parser.IterateFileInterfaces(func(interface_ *types.TypeName, parentLog *logCLI.LogCLI) error {
		log := parentLog.Debug("Analysing *types.Interface '%s'...", interface_.Name())
		code := codeInterface(interface_, fileImports, builtTypes, log)
		accumulatedCode.mockCode += code.mockCode + "\n"
		accumulatedCode.mocksFnsCode += code.mocksFnsCode + "\n"
		accumulatedCode.comparatorsCode += code.comparatorsCode + "\n"
		return nil
	})
	if e != nil {
		return nil, e
	}

	file := goFile.NewGoTestFile(fileName, packageName, packagePath)
	file.MergeImports(fileImports)
	file.AddCode(mockCodeSection)
	file.AddCode(accumulatedCode.mockCode)
	file.AddCode(mockFnCodeSection)
	file.AddCode(accumulatedCode.mocksFnsCode)
	file.AddCode(comparatorCodeSection)
	file.AddCode(accumulatedCode.comparatorsCode)

	return file, nil
}

const mockCodeSection = `
// .-------------------------.
// |    Mock Code Section    |
// '-------------------------'`
const mockFnCodeSection = `
// .-------------------------.
// |   MockFn Code Section   |
// '-------------------------'`
const comparatorCodeSection = `
// .-------------------------------.
// |    Comparator Code Section    |
// '-------------------------------'`
