package code

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goFile/goImports"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"gitlab.com/loxe-tools/go-test/internal/generate/code/templates"
	"go/types"
)

func codeParams(signature *types.Signature, fileImports *goImports.GoImports, builtTypes map[string]bool, log *logCLI.LogCLI) ([]templates.Tuple, string) {
	params := signature.Params()
	if params.Len() == 0 {
		return nil, ""
	}

	comparatorsCode := ""
	tupleParams := make([]templates.Tuple, params.Len())
	for i := 0; i < params.Len(); i++ {
		currParam := params.At(i)
		paramLog := log.Debug("Analysing parameter index %d...", i)
		paramTypeIdentifier := resolveTypeIdentifier(currParam, fileImports, paramLog)

		param := templates.Tuple{currParam.Name(), paramTypeIdentifier}
		if param.Name == "" {
			param.Name = fmt.Sprintf("p%d", i)
		}
		if signature.Variadic() && i == params.Len()-1 {
			param.TypeIdentifier = "..." + param.TypeIdentifier[2:]
		}
		tupleParams[i] = param

		_, alreadyBuilt := builtTypes[paramTypeIdentifier]
		if alreadyBuilt {
			continue
		}

		comparatorCode := templates.Comparator(paramTypeIdentifier)
		builtTypes[paramTypeIdentifier] = true
		comparatorsCode += comparatorCode + "\n"
		paramLog.Debug("%s code generated for the type '%s'...", templates.ComparatorTypeName, paramTypeIdentifier)
	}

	return tupleParams, comparatorsCode
}
