package templates

import (
	"bytes"
	"fmt"
	"text/template"
)

var mockFnRetCallArgs = func() *template.Template {
	tmpl, e := template.New("").Parse(tmplMockFnRetCallArgs)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

func MockFnRetCallArgs(functionTypeIdentifier, mockFnImportAlias string, results []Tuple) (string, error) {
	type tmplVars struct {
		SuiteTypeIdentifier string
		CustomMockFnRetCallArgsTypeName string
		ImplMockFnRetCallArgsTypeName   string
		CustomMockFnCallArgsTypeName    string
		ImplMockFnCallArgsTypeName      string
		ImplMockedFnTypeName            string
		MockFnCallArgsTypeIdentifier    string
		MockFnCallArgsTypeName          string
		Results                         []Tuple

		ReturningMethodName string
	}

	customMockFnRetCallArgsTypeName := mockFnRetCallArgsTypeName(functionTypeIdentifier)
	customMockFnCallArgsTypeName := mockFnCallArgsTypeName(functionTypeIdentifier)
	vars := tmplVars{
		fmt.Sprintf("%s.%s", mockFnImportAlias, suiteTypeName),
		customMockFnRetCallArgsTypeName,
		privatizeTypeName(customMockFnRetCallArgsTypeName),
		customMockFnCallArgsTypeName,
		privatizeTypeName(customMockFnCallArgsTypeName),
		privatizeTypeName(mockedFnTypeName(functionTypeIdentifier)),
		fmt.Sprintf("%s.%s", mockFnImportAlias, MockFnCallArgsTypeName),
		MockFnCallArgsTypeName,
		results,

		returningMethodName,
	}

	var data bytes.Buffer
	e := mockFnRetCallArgs.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

const tmplMockFnRetCallArgs = `
type {{.CustomMockFnRetCallArgsTypeName}} interface {
	{{.ReturningMethodName}}({{range .Results}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {{.CustomMockFnCallArgsTypeName}}
}
type {{.ImplMockFnRetCallArgsTypeName}} struct {
	suite {{.SuiteTypeIdentifier}}
	{{.MockFnCallArgsTypeName}} {{.MockFnCallArgsTypeIdentifier}}
}

func (__loxe_mockFn_returning_impl *{{.ImplMockFnRetCallArgsTypeName}}) {{.ReturningMethodName}}({{range .Results}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {{.CustomMockFnCallArgsTypeName}} {
	__loxe_mockFn_returning_impl.suite.Helper()
	__loxe_mockFn_returning_impl.{{.MockFnCallArgsTypeName}}.{{.ReturningMethodName}}({{range .Results}}{{.Name}}, {{end}})
	return &{{.ImplMockFnCallArgsTypeName}}{
		__loxe_mockFn_returning_impl.suite,
		__loxe_mockFn_returning_impl.{{.MockFnCallArgsTypeName}},
		&{{.ImplMockedFnTypeName}}{ __loxe_mockFn_returning_impl.suite, __loxe_mockFn_returning_impl.{{.MockFnCallArgsTypeName}} },
	}
}
`
