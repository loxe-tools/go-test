package templates

import (
	"bytes"
	"fmt"
	"text/template"
)

var mockFnRetMultiCallArgs = func() *template.Template {
	tmpl, e := template.New("").Parse(tmplMockFnRetMultiCallArgs)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

func MockFnRetMultiCallArgs(functionTypeIdentifier, mockFnImportAlias string, results []Tuple) (string, error) {
	type tmplVars struct {
		SuiteTypeIdentifier              string
		CustomMockFnRetMultiCallArgsTypeName string
		ImplMockFnRetMultiCallArgsTypeName   string
		CustomMockFnMultiCallArgsTypeName    string
		ImplMockFnMultiCallArgsTypeName      string
		MockFnMultiCallArgsTypeIdentifier    string
		MockFnMultiCallArgsTypeName          string
		Results                              []Tuple

		ReturningMethodName string
	}

	customMockFnRetMultiCallArgsTypeName := mockFnRetMultiCallArgsTypeName(functionTypeIdentifier)
	customMockFnMultiCallArgsTypeName := mockFnMultiCallArgsTypeName(functionTypeIdentifier)
	vars := tmplVars{
		fmt.Sprintf("%s.%s", mockFnImportAlias, suiteTypeName),
		customMockFnRetMultiCallArgsTypeName,
		privatizeTypeName(customMockFnRetMultiCallArgsTypeName),
		customMockFnMultiCallArgsTypeName,
		privatizeTypeName(customMockFnMultiCallArgsTypeName),
		fmt.Sprintf("%s.%s", mockFnImportAlias, MockFnMultiCallArgsTypeName),
		MockFnMultiCallArgsTypeName,
		results,

		returningMethodName,
	}

	var data bytes.Buffer
	e := mockFnRetMultiCallArgs.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

const tmplMockFnRetMultiCallArgs = `
type {{.CustomMockFnRetMultiCallArgsTypeName}} interface {
	{{.ReturningMethodName}}({{range .Results}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {{.CustomMockFnMultiCallArgsTypeName}}
}
type {{.ImplMockFnRetMultiCallArgsTypeName}} struct {
	suite {{.SuiteTypeIdentifier}}
	{{.MockFnMultiCallArgsTypeName}} {{.MockFnMultiCallArgsTypeIdentifier}}
}

func (__loxe_mockFn_ret_multi_call_impl *{{.ImplMockFnRetMultiCallArgsTypeName}}) {{.ReturningMethodName}}({{range .Results}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {{.CustomMockFnMultiCallArgsTypeName}} {
	__loxe_mockFn_ret_multi_call_impl.{{.MockFnMultiCallArgsTypeName}}.{{.ReturningMethodName}}({{range .Results}}{{.Name}}, {{end}})
	return &{{.ImplMockFnMultiCallArgsTypeName}}{ __loxe_mockFn_ret_multi_call_impl.suite, __loxe_mockFn_ret_multi_call_impl.{{.MockFnMultiCallArgsTypeName}} }
}
`
