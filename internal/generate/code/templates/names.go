package templates

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goParser/helpers"
	"strings"
)

const suiteTypeName = "MockSuite"
const mockedCallTypeName = "MockedCall"
const mockedCallsTypeName = "MockedCalls"
const mockFnMultiStubTypeName = "MockFnMultiStub"
const MockFnMultiCallsTypeName = "MockFnMultiCalls"
const newMockFnName = "NewMockFn"
const sequentialCallsTypeName = "SequentialCalls"
const nonSequentialCallsTypeName = "NonSequentialCalls"

const callMethodName = "Call"
const expectingMethodName = "Expecting"
const ignoringArgsMethodName = "IgnoringArgs"
const returningMethodName = "Returning"
const usingComparatorsMethodName = "UsingComparators"
const beforeMethodName = "Before"
const beforeAllMethodName = "BeforeAll"
const afterMethodName = "After"
const afterAllMethodName = "AfterAll"
const waitingMethodName = "Waiting"
const bindMethodName = "Bind"
const repeatMethodName = "Repeat"
const asStubMethodName = "AsStub"
const syncCallsMethodName = "SyncCalls"
const asyncCallsMethodName = "AsyncCalls"

const MockTypeName = "Mock"
func mockTypeName(interfaceTypeIdentifier string) string {
	return MockTypeName + genericLikeTypeIdentifier(interfaceTypeIdentifier)
}

const MockedTypeName = "Mocked"
func mockedTypeName(interfaceTypeIdentifier string) string {
	return MockedTypeName + genericLikeTypeIdentifier(interfaceTypeIdentifier)
}

const MockBuilderTypeName = "MockBuilder"
func mockBuilderTypeName(interfaceTypeIdentifier string) string {
	return MockBuilderTypeName + genericLikeTypeIdentifier(interfaceTypeIdentifier)
}

const MockFnAuxTypeName = "MockFnAux"
func mockFnAuxTypeName(functionTypeIdentifier string) string {
	return MockFnAuxTypeName + genericLikeTypeIdentifier(functionTypeIdentifier)
}

const MockFnTypeName = "MockFn"
func mockFnTypeName(functionTypeIdentifier string) string {
	return MockFnTypeName + genericLikeTypeIdentifier(functionTypeIdentifier)
}

const MockFnMultiTypeName = "MockFnMulti"
func mockFnMultiTypeName(functionTypeIdentifier string) string {
	return MockFnMultiTypeName + genericLikeTypeIdentifier(functionTypeIdentifier)
}

const MockFnMultiCallTypeName = "MockFnMultiCall"
func mockFnMultiCallTypeName(functionTypeIdentifier string) string {
	return MockFnMultiCallTypeName + genericLikeTypeIdentifier(functionTypeIdentifier)
}

const MockFnRetMultiCallTypeName = "MockFnRetMultiCall"
func mockFnRetMultiCallTypeName(functionTypeIdentifier string) string {
	return MockFnRetMultiCallTypeName + genericLikeTypeIdentifier(functionTypeIdentifier)
}

const MockFnMultiCallArgsTypeName = "MockFnMultiCallArgs"
func mockFnMultiCallArgsTypeName(functionTypeIdentifier string) string {
	return MockFnMultiCallArgsTypeName + genericLikeTypeIdentifier(functionTypeIdentifier)
}

const MockFnRetMultiCallArgsTypeName = "MockFnRetMultiCallArgs"
func mockFnRetMultiCallArgsTypeName(functionTypeIdentifier string) string {
	return MockFnRetMultiCallArgsTypeName + genericLikeTypeIdentifier(functionTypeIdentifier)
}

const MockFnRetCallTypeName = "MockFnRetCall"
func mockFnRetCallTypeName(functionTypeIdentifier string) string {
	return MockFnRetCallTypeName + genericLikeTypeIdentifier(functionTypeIdentifier)
}

const MockFnRetCallArgsTypeName = "MockFnRetCallArgs"
func mockFnRetCallArgsTypeName(functionTypeIdentifier string) string {
	return MockFnRetCallArgsTypeName + genericLikeTypeIdentifier(functionTypeIdentifier)
}

const MockFnStubTypeName = "MockFnStub"
func mockFnStubTypeName(functionTypeIdentifier string) string {
	return MockFnStubTypeName + genericLikeTypeIdentifier(functionTypeIdentifier)
}

const MockedFnTypeName = "MockedFn"
func mockedFnTypeName(functionTypeIdentifier string) string {
	return MockedFnTypeName + genericLikeTypeIdentifier(functionTypeIdentifier)
}

const MockFnCallsTypeName = "MockFnCalls"
func mockFnCallsTypeName(functionTypeIdentifier string) string {
	return MockFnCallsTypeName + genericLikeTypeIdentifier(functionTypeIdentifier)
}

const MockFnCallTypeName = "MockFnCall"
func mockFnCallTypeName(functionTypeIdentifier string) string {
	return MockFnCallTypeName + genericLikeTypeIdentifier(functionTypeIdentifier)
}

const MockFnCallArgsTypeName = "MockFnCallArgs"
func mockFnCallArgsTypeName(functionTypeIdentifier string) string {
	return MockFnCallArgsTypeName + genericLikeTypeIdentifier(functionTypeIdentifier)
}

const ComparatorTypeName = "Comparator"
func comparatorTypeName(typeIdentifier string) string {
	return ComparatorTypeName + genericLikeTypeIdentifier(typeIdentifier)
}

// -----

func privatizeTypeName(typeName string) string {
	return strings.ToLower(typeName[:1]) + typeName[1:]
}

func genericLikeTypeIdentifier(typeIdentifier string) string {
	return identifierToTypeName("<" + typeIdentifier + ">")
}

func SetIdentifierToTypeName(newImplementation func(string) string) error {
	if identifierToTypeNameExecuted {
		return fmt.Errorf("the 'IdentifierToTypeName' function has already been executed. Implementation replacement is allowed only before first use")
	}

	identifierToTypeName = func(typeIdentifier string) string {
		identifierToTypeNameExecuted = true
		return newImplementation(typeIdentifier)
	}
	return nil
}

var identifierToTypeName = func(typeIdentifier string) string {
	identifierToTypeNameExecuted = true
	return helpers.IdentifierToTypeName(typeIdentifier)
}

var identifierToTypeNameExecuted = false
