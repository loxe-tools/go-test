package templates

import (
	"bytes"
	"fmt"
	"text/template"
)

var mockFnMultiCall = func() *template.Template {
	tmpl, e := template.New("").Parse(tmplMockFnMultiCall)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

func MockFnMultiCall(functionTypeIdentifier, mockFnImportAlias string, params []Tuple) (string, error) {
	type tmplVars struct {
		SuiteTypeIdentifier string
		CustomMockFnMultiCallTypeName  string
		ImplMockFnMultiCallTypeName    string
		MockFnMultiCallTypeIdentifier  string
		MockFnMultiCallTypeName  string
		MockFnMultiCallsTypeIdentifier string
		MockedCallsTypeIdentifier      string
		MockedCallTypeIdentifier       string
		Comparators                    []comparator

		UsingComparatorsMethodName string
		BeforeMethodName           string
		BeforeAllMethodName        string
		AfterMethodName            string
		AfterAllMethodName         string
		WaitingMethodName          string
		BindMethodName             string
		RepeatMethodName           string
	}

	customMockFnMultiCallTypeName := mockFnMultiCallTypeName(functionTypeIdentifier)
	vars := tmplVars{
		fmt.Sprintf("%s.%s", mockFnImportAlias, suiteTypeName),
		customMockFnMultiCallTypeName,
		privatizeTypeName(customMockFnMultiCallTypeName),
		fmt.Sprintf("%s.%s", mockFnImportAlias, MockFnMultiCallTypeName),
		MockFnMultiCallTypeName,
		fmt.Sprintf("%s.%s", mockFnImportAlias, MockFnMultiCallsTypeName),
		fmt.Sprintf("%s.%s", mockFnImportAlias, mockedCallsTypeName),
		fmt.Sprintf("%s.%s", mockFnImportAlias, mockedCallTypeName),
		paramToComparator(params),

		usingComparatorsMethodName,
		beforeMethodName,
		beforeAllMethodName,
		afterMethodName,
		afterAllMethodName,
		waitingMethodName,
		bindMethodName,
		repeatMethodName,
	}

	var data bytes.Buffer
	e := mockFnMultiCall.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

const tmplMockFnMultiCall = `
type {{.CustomMockFnMultiCallTypeName}} interface {
	{{if .Comparators -}}
		{{.UsingComparatorsMethodName}}({{range .Comparators}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {{.CustomMockFnMultiCallTypeName}}
	{{end -}}
	{{.BeforeMethodName}}(call {{.MockedCallTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnMultiCallTypeName}}
	{{.BeforeAllMethodName}}(calls {{.MockedCallsTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnMultiCallTypeName}}
	{{.AfterMethodName}}(call {{.MockedCallTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnMultiCallTypeName}}
	{{.AfterAllMethodName}}(calls {{.MockedCallsTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnMultiCallTypeName}}
	{{.WaitingMethodName}}(timeout time.Duration) {{.CustomMockFnMultiCallTypeName}}
	{{.BindMethodName}}(token *{{.MockedCallTypeIdentifier}}) {{.MockedCallsTypeIdentifier}}
	{{.RepeatMethodName}}(times uint) {{.MockFnMultiCallsTypeIdentifier}} 
	{{.MockedCallsTypeIdentifier}}
}
type {{.ImplMockFnMultiCallTypeName}} struct {
	suite {{.SuiteTypeIdentifier}}
	{{.MockFnMultiCallTypeIdentifier}}
}

{{if .Comparators -}}
	func (__loxe_mockFn_multi_call_impl *{{.ImplMockFnMultiCallTypeName}}) {{.UsingComparatorsMethodName}}({{range .Comparators}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {{.CustomMockFnMultiCallTypeName}} {
		{{range $i, $curr := .Comparators -}}
			var __loxe_mockFn_multi_call_c{{$i}} func(a, b interface{}) bool
			if {{$curr.Name}} != nil {
				__loxe_mockFn_multi_call_c{{$i}} = func(expected, received interface{}) bool {
					return {{$curr.Name}}( expected.({{$curr.ElementTypeIdentifier}}), received.({{$curr.ElementTypeIdentifier}}) )
				}
			}
		{{end}}
		__loxe_mockFn_multi_call_impl.{{.MockFnMultiCallTypeName}}.{{.UsingComparatorsMethodName}}(
			{{range $i, $curr := .Comparators -}}
				__loxe_mockFn_multi_call_c{{$i}},
			{{end}}
		)
		return __loxe_mockFn_multi_call_impl
	}
{{end -}}
func (__loxe_mockFn_multi_call_impl *{{.ImplMockFnMultiCallTypeName}}) {{.BeforeMethodName}}(call {{.MockedCallTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnMultiCallTypeName}} {
	__loxe_mockFn_multi_call_impl.suite.Helper()
	__loxe_mockFn_multi_call_impl.{{.MockFnMultiCallTypeName}}.{{.BeforeMethodName}}(call, delay...)
	return __loxe_mockFn_multi_call_impl
}
func (__loxe_mockFn_multi_call_impl *{{.ImplMockFnMultiCallTypeName}}) {{.BeforeAllMethodName}}(calls {{.MockedCallsTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnMultiCallTypeName}} {
	__loxe_mockFn_multi_call_impl.suite.Helper()
	__loxe_mockFn_multi_call_impl.{{.MockFnMultiCallTypeName}}.{{.BeforeAllMethodName}}(calls, delay...)
	return __loxe_mockFn_multi_call_impl
}
func (__loxe_mockFn_multi_call_impl *{{.ImplMockFnMultiCallTypeName}}) {{.AfterMethodName}}(call {{.MockedCallTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnMultiCallTypeName}} {
	__loxe_mockFn_multi_call_impl.suite.Helper()
	__loxe_mockFn_multi_call_impl.{{.MockFnMultiCallTypeName}}.{{.AfterMethodName}}(call, delay...)
	return __loxe_mockFn_multi_call_impl
}
func (__loxe_mockFn_multi_call_impl *{{.ImplMockFnMultiCallTypeName}}) {{.AfterAllMethodName}}(calls {{.MockedCallsTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnMultiCallTypeName}} {
	__loxe_mockFn_multi_call_impl.suite.Helper()
	__loxe_mockFn_multi_call_impl.{{.MockFnMultiCallTypeName}}.{{.AfterAllMethodName}}(calls, delay...)
	return __loxe_mockFn_multi_call_impl
}
func (__loxe_mockFn_multi_call_impl *{{.ImplMockFnMultiCallTypeName}}) {{.WaitingMethodName}}(timeout time.Duration) {{.CustomMockFnMultiCallTypeName}} {
	__loxe_mockFn_multi_call_impl.{{.MockFnMultiCallTypeName}}.{{.WaitingMethodName}}(timeout)
	return __loxe_mockFn_multi_call_impl
}
`
