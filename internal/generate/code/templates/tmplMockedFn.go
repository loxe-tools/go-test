package templates

import (
	"bytes"
	"fmt"
	"text/template"
)

var mockedFn = func() *template.Template {
	funcMap := template.FuncMap{"subOne": func(i int) int { return i - 1 }}
	tmpl, e := template.New("").Funcs(funcMap).Parse(tmplMockedFn)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

func MockedFn(functionTypeIdentifier, mockFnImportAlias string, params, results []Tuple) (string, error) {
	type tmplVars struct {
		SuiteTypeIdentifier string
		CustomMockedFnTypeName string
		ImplMockedFnTypeName string
		MockedFnTypeIdentifier string
		MockedFnTypeName string
		Params []Tuple
		Results []Tuple

		CallMethodName string
	}

	customMockedFnTypeName := mockedFnTypeName(functionTypeIdentifier)
	vars := tmplVars{
		fmt.Sprintf("%s.%s", mockFnImportAlias, suiteTypeName),
		 customMockedFnTypeName,
		   privatizeTypeName(customMockedFnTypeName),
		 fmt.Sprintf("%s.%s", mockFnImportAlias, MockedFnTypeName),
		       MockedFnTypeName,
		                 params,
		                results,

		         callMethodName,
	}

	var data bytes.Buffer
	e := mockedFn.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

const tmplMockedFn = `
type {{.CustomMockedFnTypeName}} interface {
	{{.CallMethodName}}({{range .Params}}{{.Name}} {{.TypeIdentifier}}, {{end}}) ({{range .Results}}{{.Name}} {{.TypeIdentifier}}, {{end}})
}
type {{.ImplMockedFnTypeName}} struct {
	suite {{.SuiteTypeIdentifier}}
	{{.MockedFnTypeName}} {{.MockedFnTypeIdentifier}}
}

func (__loxe_mockedFn_impl *{{.ImplMockedFnTypeName}}) {{.CallMethodName}}({{range .Params}}{{.Name}} {{.TypeIdentifier}}, {{end}}) ({{range .Results}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {
	__loxe_mockedFn_impl.suite.Helper()
	{{if .Results -}}
		__loxe_mockedFn_call_results := __loxe_mockedFn_impl.{{.MockedFnTypeName}}.{{.CallMethodName}}({{range .Params}}{{.Name}}, {{end}})
		{{$lastIdx := len .Results | subOne -}}
		return {{range $i, $result := .Results}}__loxe_mockedFn_call_results[{{$i}}].({{$result.TypeIdentifier}}){{if ne $lastIdx $i}}, {{end}} {{end}}
	{{else -}}
		__loxe_mockedFn_impl.{{.MockedFnTypeName}}.{{.CallMethodName}}({{range .Params}}{{.Name}}, {{end}})
	{{end -}}
}
`
