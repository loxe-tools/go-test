package templates

import (
	"bytes"
	"fmt"
	"text/template"
)

var mockFnRetMultiCall = func() *template.Template {
	tmpl, e := template.New("").Parse(tmplMockFnRetMultiCall)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

func MockFnRetMultiCall(functionTypeIdentifier, mockFnImportAlias string, results []Tuple) (string, error) {
	type tmplVars struct {
		SuiteTypeIdentifier              string
		CustomMockFnRetMultiCallTypeName string
		ImplMockFnRetMultiCallTypeName   string
		CustomMockFnMultiCallTypeName    string
		ImplMockFnMultiCallTypeName      string
		MockFnMultiCallTypeIdentifier    string
		MockFnMultiCallTypeName          string
		Results                          []Tuple

		ReturningMethodName string
	}

	customMockFnRetMultiCallTypeName := mockFnRetMultiCallTypeName(functionTypeIdentifier)
	customMockFnMultiCallTypeName := mockFnMultiCallTypeName(functionTypeIdentifier)
	vars := tmplVars{
		fmt.Sprintf("%s.%s", mockFnImportAlias, suiteTypeName),
		customMockFnRetMultiCallTypeName,
		privatizeTypeName(customMockFnRetMultiCallTypeName),
		customMockFnMultiCallTypeName,
		privatizeTypeName(customMockFnMultiCallTypeName),
		fmt.Sprintf("%s.%s", mockFnImportAlias, MockFnMultiCallTypeName),
		MockFnMultiCallTypeName,
		results,

		returningMethodName,
	}

	var data bytes.Buffer
	e := mockFnRetMultiCall.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

const tmplMockFnRetMultiCall = `
type {{.CustomMockFnRetMultiCallTypeName}} interface {
	{{.ReturningMethodName}}({{range .Results}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {{.CustomMockFnMultiCallTypeName}}
}
type {{.ImplMockFnRetMultiCallTypeName}} struct {
	suite {{.SuiteTypeIdentifier}}
	{{.MockFnMultiCallTypeName}} {{.MockFnMultiCallTypeIdentifier}}
}

func (__loxe_mockFn_ret_multi_call_impl *{{.ImplMockFnRetMultiCallTypeName}}) {{.ReturningMethodName}}({{range .Results}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {{.CustomMockFnMultiCallTypeName}} {
	__loxe_mockFn_ret_multi_call_impl.{{.MockFnMultiCallTypeName}}.{{.ReturningMethodName}}({{range .Results}}{{.Name}}, {{end}})
	return &{{.ImplMockFnMultiCallTypeName}}{ __loxe_mockFn_ret_multi_call_impl.suite, __loxe_mockFn_ret_multi_call_impl.{{.MockFnMultiCallTypeName}} }
}
`
