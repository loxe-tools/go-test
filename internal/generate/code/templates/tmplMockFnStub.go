package templates

import (
	"bytes"
	"fmt"
	"text/template"
)

var mockFnStub = func() *template.Template {
	tmpl, e := template.New("").Parse(mockFnStubTmpl)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

func MockFnStub(functionTypeIdentifier, mockFnImportAlias string) (string, error) {
	type tmplVars struct {
		SuiteTypeIdentifier              string
		CustomMockFnStubTypeName string
		ImplMockFnStubTypeName string
		CustomMockedFnTypeName string
		ImplMockedFnTypeName string
		MockFnStubTypeIdentifier string
		MockFnStubTypeName string

		RepeatMethodName string
	}

	customMockFnStubTypeName := mockFnStubTypeName(functionTypeIdentifier)
	customMockedFnTypeName := mockedFnTypeName(functionTypeIdentifier)
	vars := tmplVars{
		fmt.Sprintf("%s.%s", mockFnImportAlias, suiteTypeName),
		customMockFnStubTypeName,
		privatizeTypeName(customMockFnStubTypeName),
		customMockedFnTypeName,
		privatizeTypeName(customMockedFnTypeName),
		fmt.Sprintf("%s.%s", mockFnImportAlias, MockFnStubTypeName),
		MockFnStubTypeName,

		repeatMethodName,
	}

	var data bytes.Buffer
	e := mockFnStub.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

const mockFnStubTmpl = `
type {{.CustomMockFnStubTypeName}} interface {
	{{.RepeatMethodName}}(times uint) {{.CustomMockedFnTypeName}}
	{{.CustomMockedFnTypeName}}
}
type {{.ImplMockFnStubTypeName}} struct {
	suite {{.SuiteTypeIdentifier}}
	{{.MockFnStubTypeName}} {{.MockFnStubTypeIdentifier}}
	*{{.ImplMockedFnTypeName}}
}

func (__loxe_mockFn_stub_impl *{{.ImplMockFnStubTypeName}}) {{.RepeatMethodName}}(times uint) {{.CustomMockedFnTypeName}} {
	__loxe_mockFn_stub_impl.suite.Helper()
	__loxe_mockFn_stub_impl.{{.MockFnStubTypeName}}.{{.RepeatMethodName}}(times)
	return __loxe_mockFn_stub_impl.{{.ImplMockedFnTypeName}}
}
`
