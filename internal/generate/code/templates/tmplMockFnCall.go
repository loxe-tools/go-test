package templates

import (
	"bytes"
	"fmt"
	"text/template"
)

var mockFnCall = func() *template.Template {
	tmpl, e := template.New("").Parse(tmplMockFnCall)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

func MockFnCall(functionTypeIdentifier, mockFnImportAlias string, params []Tuple) (string, error) {
	type tmplVars struct {
		SuiteTypeIdentifier string
		CustomMockFnCallTypeName  string
		ImplMockFnCallTypeName    string
		CustomMockFnCallsTypeName string
		ImplMockFnCallsTypeName   string
		CustomMockedFnTypeName    string
		ImplMockedFnTypeName      string
		MockedCallsTypeIdentifier string
		MockedCallTypeIdentifier  string
		MockFnCallTypeIdentifier  string
		MockFnCallTypeName        string
		Comparators               []comparator

		UsingComparatorsMethodName string
		BeforeMethodName           string
		BeforeAllMethodName        string
		AfterMethodName            string
		AfterAllMethodName         string
		WaitingMethodName          string
		BindMethodName             string
		RepeatMethodName           string
	}

	customMockFnCallTypeName := mockFnCallTypeName(functionTypeIdentifier)
	customMockFnCallsTypeName := mockFnCallsTypeName(functionTypeIdentifier)
	customMockedFnTypeName := mockedFnTypeName(functionTypeIdentifier)
	vars := tmplVars{
		fmt.Sprintf("%s.%s", mockFnImportAlias, suiteTypeName),
		customMockFnCallTypeName,
		privatizeTypeName(customMockFnCallTypeName),
		customMockFnCallsTypeName,
		privatizeTypeName(customMockFnCallsTypeName),
		customMockedFnTypeName,
		privatizeTypeName(customMockedFnTypeName),
		fmt.Sprintf("%s.%s", mockFnImportAlias, mockedCallsTypeName),
		fmt.Sprintf("%s.%s", mockFnImportAlias, mockedCallTypeName),
		fmt.Sprintf("%s.%s", mockFnImportAlias, MockFnCallTypeName),
		MockFnCallTypeName,
		paramToComparator(params),

		usingComparatorsMethodName,
		beforeMethodName,
		beforeAllMethodName,
		afterMethodName,
		afterAllMethodName,
		waitingMethodName,
		bindMethodName,
		repeatMethodName,
	}

	var data bytes.Buffer
	e := mockFnCall.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

const tmplMockFnCall = `
type {{.CustomMockFnCallTypeName}} interface {
	{{if .Comparators -}}
		{{.UsingComparatorsMethodName}}({{range .Comparators}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {{.CustomMockFnCallTypeName}}
	{{end -}}
	{{.BeforeMethodName}}(call {{.MockedCallTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnCallTypeName}}
	{{.BeforeAllMethodName}}(calls {{.MockedCallsTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnCallTypeName}}
	{{.AfterMethodName}}(call {{.MockedCallTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnCallTypeName}}
	{{.AfterAllMethodName}}(calls {{.MockedCallsTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnCallTypeName}}
	{{.WaitingMethodName}}(timeout time.Duration) {{.CustomMockFnCallTypeName}}
	{{.BindMethodName}}(token *{{.MockedCallTypeIdentifier}}) {{.CustomMockedFnTypeName}}
	{{.RepeatMethodName}}(times uint) {{.CustomMockFnCallsTypeName}}
	{{.CustomMockedFnTypeName}}
}
type {{.ImplMockFnCallTypeName}} struct {
	suite {{.SuiteTypeIdentifier}}
	{{.MockFnCallTypeName}} {{.MockFnCallTypeIdentifier}}
	{{.CustomMockedFnTypeName}}
}

{{if .Comparators -}}
	func (__loxe_mockFn_call_impl *{{.ImplMockFnCallTypeName}}) {{.UsingComparatorsMethodName}}({{range .Comparators}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {{.CustomMockFnCallTypeName}} {
		{{range $i, $curr := .Comparators -}}
			var __loxe_mockFn_multi_call_c{{$i}} func(a, b interface{}) bool
			if {{$curr.Name}} != nil {
				__loxe_mockFn_multi_call_c{{$i}} = func(expected, received interface{}) bool {
					return {{$curr.Name}}( expected.({{$curr.ElementTypeIdentifier}}), received.({{$curr.ElementTypeIdentifier}}) )
				}
			}
		{{end}}
		__loxe_mockFn_call_impl.{{.MockFnCallTypeName}}.{{.UsingComparatorsMethodName}}(
			{{range $i, $curr := .Comparators -}}
				__loxe_mockFn_multi_call_c{{$i}},
			{{end}}
		)
		return __loxe_mockFn_call_impl
	}
{{end -}}
func (__loxe_mockFn_call_impl *{{.ImplMockFnCallTypeName}}) {{.BeforeMethodName}}(call {{.MockedCallTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnCallTypeName}} {
	__loxe_mockFn_call_impl.suite.Helper()
	__loxe_mockFn_call_impl.{{.MockFnCallTypeName}}.{{.BeforeMethodName}}(call, delay...)
	return __loxe_mockFn_call_impl
}
func (__loxe_mockFn_call_impl *{{.ImplMockFnCallTypeName}}) {{.BeforeAllMethodName}}(calls {{.MockedCallsTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnCallTypeName}} {
	__loxe_mockFn_call_impl.suite.Helper()
	__loxe_mockFn_call_impl.{{.MockFnCallTypeName}}.{{.BeforeAllMethodName}}(calls, delay...)
	return __loxe_mockFn_call_impl
}
func (__loxe_mockFn_call_impl *{{.ImplMockFnCallTypeName}}) {{.AfterMethodName}}(call {{.MockedCallTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnCallTypeName}} {
	__loxe_mockFn_call_impl.suite.Helper()
	__loxe_mockFn_call_impl.{{.MockFnCallTypeName}}.{{.AfterMethodName}}(call, delay...)
	return __loxe_mockFn_call_impl
}
func (__loxe_mockFn_call_impl *{{.ImplMockFnCallTypeName}}) {{.AfterAllMethodName}}(calls {{.MockedCallsTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnCallTypeName}} {
	__loxe_mockFn_call_impl.suite.Helper()
	__loxe_mockFn_call_impl.{{.MockFnCallTypeName}}.{{.AfterAllMethodName}}(calls, delay...)
	return __loxe_mockFn_call_impl
}
func (__loxe_mockFn_call_impl *{{.ImplMockFnCallTypeName}}) {{.WaitingMethodName}}(timeout time.Duration) {{.CustomMockFnCallTypeName}} {
	__loxe_mockFn_call_impl.{{.MockFnCallTypeName}}.{{.WaitingMethodName}}(timeout)
	return __loxe_mockFn_call_impl
}
func (__loxe_mockFn_call_impl *{{.ImplMockFnCallTypeName}}) {{.BindMethodName}}(token *{{.MockedCallTypeIdentifier}}) {{.CustomMockedFnTypeName}} {
	__loxe_mockFn_call_impl.suite.Helper()
	__loxe_mockFn_call_impl.{{.MockFnCallTypeName}}.{{.BindMethodName}}(token)
	return __loxe_mockFn_call_impl
}
func (__loxe_mockFn_call_impl *{{.ImplMockFnCallTypeName}}) {{.RepeatMethodName}}(times uint) {{.CustomMockFnCallsTypeName}} {
	__loxe_mockFn_call_impl.suite.Helper()
	return &{{.ImplMockFnCallsTypeName}}{
		__loxe_mockFn_call_impl.suite,
		__loxe_mockFn_call_impl.{{.MockFnCallTypeName}}.{{.RepeatMethodName}}(times),
		__loxe_mockFn_call_impl,
	}
}
`
