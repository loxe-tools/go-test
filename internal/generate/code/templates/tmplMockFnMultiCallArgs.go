package templates

import (
	"bytes"
	"fmt"
	"text/template"
)

var mockFnMultiCallArgs = func() *template.Template {
	tmpl, e := template.New("").Parse(tmplMockFnMultiCallArgs)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

func MockFnMultiCallArgs(functionTypeIdentifier, mockFnImportAlias string) (string, error) {
	type tmplVars struct {
		SuiteTypeIdentifier string
		CustomMockFnMultiCallArgsTypeName string
		ImplMockFnMultiCallArgsTypeName   string
		MockFnMultiCallArgsTypeIdentifier string
		MockFnMultiCallArgsTypeName       string
		MockFnMultiCallsTypeIdentifier    string
		MockedCallsTypeIdentifier         string
		MockedCallTypeIdentifier          string

		BeforeMethodName    string
		BeforeAllMethodName string
		AfterMethodName     string
		AfterAllMethodName  string
		WaitingMethodName   string
		BindMethodName      string
		RepeatMethodName    string
	}

	customMockFnMultiCallArgsTypeName := mockFnMultiCallArgsTypeName(functionTypeIdentifier)
	vars := tmplVars{
		fmt.Sprintf("%s.%s", mockFnImportAlias, suiteTypeName),
		customMockFnMultiCallArgsTypeName,
		privatizeTypeName(customMockFnMultiCallArgsTypeName),
		fmt.Sprintf("%s.%s", mockFnImportAlias, MockFnMultiCallArgsTypeName),
		MockFnMultiCallArgsTypeName,
		fmt.Sprintf("%s.%s", mockFnImportAlias, MockFnMultiCallsTypeName),
		fmt.Sprintf("%s.%s", mockFnImportAlias, mockedCallsTypeName),
		fmt.Sprintf("%s.%s", mockFnImportAlias, mockedCallTypeName),

		beforeMethodName,
		beforeAllMethodName,
		afterMethodName,
		afterAllMethodName,
		waitingMethodName,
		bindMethodName,
		repeatMethodName,
	}

	var data bytes.Buffer
	e := mockFnMultiCallArgs.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

const tmplMockFnMultiCallArgs = `
type {{.CustomMockFnMultiCallArgsTypeName}} interface {
	{{.BeforeMethodName}}(call {{.MockedCallTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnMultiCallArgsTypeName}}
	{{.BeforeAllMethodName}}(calls {{.MockedCallsTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnMultiCallArgsTypeName}}
	{{.AfterMethodName}}(call {{.MockedCallTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnMultiCallArgsTypeName}}
	{{.AfterAllMethodName}}(calls {{.MockedCallsTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnMultiCallArgsTypeName}}
	{{.WaitingMethodName}}(timeout time.Duration) {{.CustomMockFnMultiCallArgsTypeName}}
	{{.BindMethodName}}(token *{{.MockedCallTypeIdentifier}}) {{.MockedCallsTypeIdentifier}}
	{{.RepeatMethodName}}(times uint) {{.MockFnMultiCallsTypeIdentifier}} 
	{{.MockedCallsTypeIdentifier}}
}
type {{.ImplMockFnMultiCallArgsTypeName}} struct {
	suite {{.SuiteTypeIdentifier}}
	{{.MockFnMultiCallArgsTypeIdentifier}}
}

func (__loxe_mockFn_multi_call_args_impl *{{.ImplMockFnMultiCallArgsTypeName}}) {{.BeforeMethodName}}(call {{.MockedCallTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnMultiCallArgsTypeName}} {
	__loxe_mockFn_multi_call_args_impl.suite.Helper()
	__loxe_mockFn_multi_call_args_impl.{{.MockFnMultiCallArgsTypeName}}.{{.BeforeMethodName}}(call, delay...)
	return __loxe_mockFn_multi_call_args_impl
}
func (__loxe_mockFn_multi_call_args_impl *{{.ImplMockFnMultiCallArgsTypeName}}) {{.BeforeAllMethodName}}(calls {{.MockedCallsTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnMultiCallArgsTypeName}} {
	__loxe_mockFn_multi_call_args_impl.suite.Helper()
	__loxe_mockFn_multi_call_args_impl.{{.MockFnMultiCallArgsTypeName}}.{{.BeforeAllMethodName}}(calls, delay...)
	return __loxe_mockFn_multi_call_args_impl
}
func (__loxe_mockFn_multi_call_args_impl *{{.ImplMockFnMultiCallArgsTypeName}}) {{.AfterMethodName}}(call {{.MockedCallTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnMultiCallArgsTypeName}} {
	__loxe_mockFn_multi_call_args_impl.suite.Helper()
	__loxe_mockFn_multi_call_args_impl.{{.MockFnMultiCallArgsTypeName}}.{{.AfterMethodName}}(call, delay...)
	return __loxe_mockFn_multi_call_args_impl
}
func (__loxe_mockFn_multi_call_args_impl *{{.ImplMockFnMultiCallArgsTypeName}}) {{.AfterAllMethodName}}(calls {{.MockedCallsTypeIdentifier}}, delay ...time.Duration) {{.CustomMockFnMultiCallArgsTypeName}} {
	__loxe_mockFn_multi_call_args_impl.suite.Helper()
	__loxe_mockFn_multi_call_args_impl.{{.MockFnMultiCallArgsTypeName}}.{{.AfterAllMethodName}}(calls, delay...)
	return __loxe_mockFn_multi_call_args_impl
}
func (__loxe_mockFn_multi_call_args_impl *{{.ImplMockFnMultiCallArgsTypeName}}) {{.WaitingMethodName}}(timeout time.Duration) {{.CustomMockFnMultiCallArgsTypeName}} {
	__loxe_mockFn_multi_call_args_impl.{{.MockFnMultiCallArgsTypeName}}.{{.WaitingMethodName}}(timeout)
	return __loxe_mockFn_multi_call_args_impl
}
`
