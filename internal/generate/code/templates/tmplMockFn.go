package templates

import (
	"bytes"
	"fmt"
	"text/template"
)

var mockFn = func() *template.Template {
	tmpl, e := template.New("").Parse(mockFnTmpl)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

func MockFn(functionTypeIdentifier, mockFnImportAlias string, params, results []Tuple) (string, error) {
	type tmplVars struct {
		SuiteTypeIdentifier string
		CustomMockFnCallsTypeName        string
		ImplMockFnCallsTypeName          string
		ImplMockedFnTypeName             string
		CustomMockFnAuxTypeName          string
		CustomMockFnTypeName             string
		ImplMockFnTypeName               string
		CustomMockFnMultiTypeName        string
		ImplMockFnMultiTypeName          string
		CustomMockFnRetCallArgsTypeName  string
		ImplMockFnRetCallArgsTypeName    string
		CustomMockFnCallArgsTypeName     string
		ImplMockFnCallArgsTypeName       string
		CustomMockFnRetCallTypeName      string
		ImplMockFnRetCallTypeName        string
		CustomMockFnCallTypeName         string
		ImplMockFnCallTypeName           string
		CustomMockFnStubTypeName         string
		ImplMockFnStubTypeName           string
		SequentialCallsTypeIdentifier    string
		NonSequentialCallsTypeIdentifier string
		MockFnMultiTypeIdentifier        string
		NewMockFnIdentifier       string
		Params                           []Tuple
		Results                          []Tuple

		AsyncCallsMethodName   string
		IgnoringArgsMethodName string
		ExpectingMethodName    string
		AsStubMethodName       string
		SyncCallsMethodName    string
	}

	customMockFnCallsTypeName := mockFnCallsTypeName(functionTypeIdentifier)
	customMockFnTypeName := mockFnTypeName(functionTypeIdentifier)
	customMockFnMultiTypeName := mockFnMultiTypeName(functionTypeIdentifier)
	customMockFnRetCallArgsTypeName := mockFnRetCallArgsTypeName(functionTypeIdentifier)
	customMockFnCallArgsTypeName := mockFnCallArgsTypeName(functionTypeIdentifier)
	customMockFnRetCallTypeName := mockFnRetCallTypeName(functionTypeIdentifier)
	customMockFnCallTypeName := mockFnCallTypeName(functionTypeIdentifier)
	customMockFnStubTypeName := mockFnStubTypeName(functionTypeIdentifier)
	vars := tmplVars{
		fmt.Sprintf("%s.%s", mockFnImportAlias, suiteTypeName),
		customMockFnCallsTypeName,
		privatizeTypeName(customMockFnCallsTypeName),
		privatizeTypeName(mockedFnTypeName(functionTypeIdentifier)),
		mockFnAuxTypeName(functionTypeIdentifier),
		customMockFnTypeName,
		privatizeTypeName(customMockFnTypeName),
		customMockFnMultiTypeName,
		privatizeTypeName(customMockFnMultiTypeName),
		customMockFnRetCallArgsTypeName,
		privatizeTypeName(customMockFnRetCallArgsTypeName),
		customMockFnCallArgsTypeName,
		privatizeTypeName(customMockFnCallArgsTypeName),
		customMockFnRetCallTypeName,
		privatizeTypeName(customMockFnRetCallTypeName),
		customMockFnCallTypeName,
		privatizeTypeName(customMockFnCallTypeName),
		customMockFnStubTypeName,
		privatizeTypeName(customMockFnStubTypeName),
		fmt.Sprintf("%s.%s", mockFnImportAlias, sequentialCallsTypeName),
		fmt.Sprintf("%s.%s", mockFnImportAlias, nonSequentialCallsTypeName),
		fmt.Sprintf("%s.%s", mockFnImportAlias, MockFnMultiTypeName),
		fmt.Sprintf("%s.%s", mockFnImportAlias, newMockFnName),
		params,
		results,

		asyncCallsMethodName,
		ignoringArgsMethodName,
		expectingMethodName,
		asStubMethodName,
		syncCallsMethodName,
	}

	var data bytes.Buffer
	e := mockFn.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

const mockFnTmpl = `
type {{.CustomMockFnAuxTypeName}} interface {
	{{.AsyncCallsMethodName}}(callback func(builder {{.CustomMockFnMultiTypeName}}) {{.NonSequentialCallsTypeIdentifier}}) {{.CustomMockFnCallsTypeName}}
	{{.CustomMockFnCallsTypeName}}
}
type {{.CustomMockFnTypeName}} interface {
	{{if .Params -}}
		{{.IgnoringArgsMethodName}}() {{if .Results}} {{.CustomMockFnRetCallArgsTypeName}} {{else}} {{.CustomMockFnCallArgsTypeName}} {{end}}
	{{end -}}
	{{.ExpectingMethodName}}({{range .Params}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {{if .Results}} {{.CustomMockFnRetCallTypeName}} {{else}} {{.CustomMockFnCallTypeName}} {{end}}
	{{.AsStubMethodName}}({{range .Results}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {{.CustomMockFnStubTypeName}}
	{{.SyncCallsMethodName}}(callback func(builder {{.CustomMockFnMultiTypeName}}) {{.SequentialCallsTypeIdentifier}}) {{.CustomMockFnAuxTypeName}}
	{{.AsyncCallsMethodName}}(callback func(builder {{.CustomMockFnMultiTypeName}}) {{.NonSequentialCallsTypeIdentifier}}) {{.CustomMockFnCallsTypeName}}
}
type {{.ImplMockFnTypeName}} struct {
	suite {{.SuiteTypeIdentifier}}
}

{{if .Params -}}
	func (__loxe_mockFn_impl *{{.ImplMockFnTypeName}}) {{.IgnoringArgsMethodName}}() {{if .Results}} {{.CustomMockFnRetCallArgsTypeName}} {{else}} {{.CustomMockFnCallArgsTypeName}} {{end}} {
		__loxe_base_result := {{.NewMockFnIdentifier}}(__loxe_mockFn_impl.suite, 1).{{.IgnoringArgsMethodName}}()
		{{if .Results -}}
			return &{{.ImplMockFnRetCallArgsTypeName}}{ __loxe_mockFn_impl.suite, __loxe_base_result }
		{{else -}}
			return &{{.ImplMockFnCallArgsTypeName}}{
				__loxe_mockFn_impl.suite,
				__loxe_base_result,
				&{{.ImplMockedFnTypeName}}{ __loxe_mockFn_impl.suite, __loxe_base_result },
			}
		{{end -}}
	}
{{end -}}
func (__loxe_mockFn_impl *{{.ImplMockFnTypeName}}) {{.ExpectingMethodName}}({{range .Params}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {{if .Results}} {{.CustomMockFnRetCallTypeName}} {{else}} {{.CustomMockFnCallTypeName}} {{end}} {
	__loxe_base_result := {{.NewMockFnIdentifier}}(__loxe_mockFn_impl.suite, 1).{{.ExpectingMethodName}}({{range .Params}}{{.Name}}, {{end}})
	{{if .Results -}}
		return &{{.ImplMockFnRetCallTypeName}}{ __loxe_mockFn_impl.suite, __loxe_base_result }
	{{else -}}
		return &{{.ImplMockFnCallTypeName}}{
			__loxe_mockFn_impl.suite,
			__loxe_base_result,
			&{{.ImplMockedFnTypeName}}{ __loxe_mockFn_impl.suite, __loxe_base_result },
		}
	{{end -}}
}
func (__loxe_mockFn_impl *{{.ImplMockFnTypeName}}) {{.AsStubMethodName}}({{range .Results}}{{.Name}} {{.TypeIdentifier}}, {{end}}) {{.CustomMockFnStubTypeName}} {
	__loxe_base_result := {{.NewMockFnIdentifier}}(__loxe_mockFn_impl.suite, 1).{{.AsStubMethodName}}({{range .Results}}{{.Name}}, {{end}})
	return &{{.ImplMockFnStubTypeName}}{
		__loxe_mockFn_impl.suite, 
		__loxe_base_result,
		&{{.ImplMockedFnTypeName}}{ __loxe_mockFn_impl.suite, __loxe_base_result },
	}
}
func (__loxe_mockFn_impl *{{.ImplMockFnTypeName}}) {{.SyncCallsMethodName}}(callback func(builder {{.CustomMockFnMultiTypeName}}) {{.SequentialCallsTypeIdentifier}}) {{.CustomMockFnAuxTypeName}} {
	__loxe_mockFn_impl.suite.Helper()
	if callback == nil {
		{{.NewMockFnIdentifier}}(__loxe_mockFn_impl.suite, 1).{{.SyncCallsMethodName}}(nil)
		return nil
	}

	__loxe_mockFn_sync_result := {{.NewMockFnIdentifier}}(__loxe_mockFn_impl.suite, 1).{{.SyncCallsMethodName}}(func(builder {{.MockFnMultiTypeIdentifier}}) {{.SequentialCallsTypeIdentifier}} {
		return callback(&{{.ImplMockFnMultiTypeName}}{__loxe_mockFn_impl.suite, builder})
	})
	return struct{
		*{{.ImplMockFnTypeName}}
		*{{.ImplMockFnCallsTypeName}}
	}{
		__loxe_mockFn_impl,
		&{{.ImplMockFnCallsTypeName}}{
			__loxe_mockFn_impl.suite,
			__loxe_mockFn_sync_result,
			&{{.ImplMockedFnTypeName}}{ __loxe_mockFn_impl.suite, __loxe_mockFn_sync_result },
		},
	}
}
func (__loxe_mockFn_impl *{{.ImplMockFnTypeName}}) {{.AsyncCallsMethodName}}(callback func(builder {{.CustomMockFnMultiTypeName}}) {{.NonSequentialCallsTypeIdentifier}}) {{.CustomMockFnCallsTypeName}} {
	__loxe_mockFn_impl.suite.Helper()
	if callback == nil {
		{{.NewMockFnIdentifier}}(__loxe_mockFn_impl.suite, 1).{{.AsyncCallsMethodName}}(nil)
		return nil
	}

	__loxe_mockFn_async_result := {{.NewMockFnIdentifier}}(__loxe_mockFn_impl.suite, 1).{{.AsyncCallsMethodName}}(func(builder {{.MockFnMultiTypeIdentifier}}) {{.NonSequentialCallsTypeIdentifier}} {
		return callback(&{{.ImplMockFnMultiTypeName}}{__loxe_mockFn_impl.suite, builder})
	})
	return &{{.ImplMockFnCallsTypeName}}{
		__loxe_mockFn_impl.suite,
		__loxe_mockFn_async_result,
		&{{.ImplMockedFnTypeName}}{ __loxe_mockFn_impl.suite, __loxe_mockFn_async_result },
	}
}
`
