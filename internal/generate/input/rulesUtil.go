package input

import (
	"fmt"
	"golang.org/x/tools/go/packages"
	"os"
	"regexp"
	"strings"
)

func notRequired(s string) error {
	return Notᐸstringᐳ(required, fmt.Errorf("This flag is not required. More info use --help"))(s)
}

func validFilePackage(s string) error {
	cfg := &packages.Config{Mode: packages.NeedFiles | packages.NeedSyntax}
	pkgs, e := packages.Load(cfg, "file="+s)
	if e != nil {
		return fmt.Errorf("Cannot parse the given package: %v", e)
	}
	if len(pkgs) != 1 {
		return fmt.Errorf("Cannot parse more than one package at a time")
	}
	if len(pkgs[0].Errors) != 0 {
		return fmt.Errorf("The given package contains errors: %v", pkgs[0].Errors[0])
	}

	return nil
}

func required(s string) error {
	if strIsOnlyWhiteSpace(s) {
		return fmt.Errorf("This flag is required. More info use --help")
	}
	return nil
}

func validGoIdentifier(s string) error {
	if !goIdentifierRegexp.MatchString(s) {
		return fmt.Errorf("This value is not a valid GO identifier (see https://golang.org/ref/spec#identifier)")
	}
	return nil
}

func validFilepath(s string) error {
	file, e := os.Open(s)
	if e != nil {
		return fmt.Errorf("Cannot open this path: %v", e)
	}
	defer file.Close()
	stats, e := file.Stat()
	if e != nil {
		return fmt.Errorf("Cannot check this path info: %v", e)
	}
	if stats.IsDir() {
		return fmt.Errorf("Not a file path (points to a directory)")
	}

	return nil
}

// -----

func strIsOnlyWhiteSpace(s string) bool { return strings.TrimSpace(s) == "" }

var goIdentifierRegexp = func() *regexp.Regexp {
	reg, e := regexp.Compile("^(\\pL|_)(?:\\pL|_|\\p{Nd})*$")
	if e != nil {
		panic(e)
	}
	return reg
}()
