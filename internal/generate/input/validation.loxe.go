/*
█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████
██                                                                                                                 ██
██                                  ▄██▄        ▄██▄                                                               ██
██                                 ██████      ██████                                                              ██
██        ████                      ▀██▀        ▀██▀                                                               ██
██        ████                                                                                                     ██
██        ████                       ▄████████████▄          ██████       ██████          ▄████████████▄           ██
██        ████                      ████████████████          ██████     ██████          ████████████████▄         ██
██        ████                    ▄█████▀      ▀█████▄         ██████   ██████          █████▀       ▀█████        ██
██        ████                    █████          █████          ██████ ██████          ████████████████████        ██
██        ████                    █████          █████           ███████████           █████                       ██
██        ████                     █████▄      ▄█████             █████████             █████▄       ▄█████        ██
██        ███████████████▄          ▀██████████████▀               ███████               ▀███████████████▀         ██
██        █████████████████▄          ▀██████████▀                █████████                ▀███████████▀           ██
██                                                               ███████████                                       ██
██                                                              ██████ ██████                                      ██
█████████████████████████████████████████████████████████      ██████   ██████      █████████████████████████████████
                                                              ██████     ██████
                                                             ██████       ██████

                            Löxe - Criação e Desenvolvimento (Santa Catarina, Brasil)
                     FILE GENERATED AUTOMATICALLY BY gitlab.com/loxe-tools/go-validation v1.0.41
*/
package input

import (
	validator "gitlab.com/loxe-tools/go-validation"
)

func NewSchemaᐸArgumentsᐳ() SchemaᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		return validator.NewLoxeValidator()
	}
}

func NewTestSchemaᐸArgumentsᐳ() TestSchemaᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {}
}

type SchemaᐸArgumentsᐳ func(struct_ Arguments) *validator.LoxeValidator
type TestSchemaᐸArgumentsᐳ func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ)
type SchemaFieldsᐸArgumentsᐳ func(struct_ Arguments) *validator.LoxeValidator
type TestSchemaFieldsᐸArgumentsᐳ func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ)
type SchemaRootedFieldsᐸArgumentsᐳ func(struct_ Arguments) *validator.LoxeValidator
type TestSchemaRootedFieldsᐸArgumentsᐳ func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ)

func (prevFn SchemaᐸArgumentsᐳ) RootRule(rule func(Arguments) error) SchemaᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyRootError(rule(struct_))
		return v
	}
}

func (prevFn TestSchemaᐸArgumentsᐳ) RootRule(test TestRuleᐸArgumentsᐳ) TestSchemaᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value Arguments) error {
			return schema(value).RootError()
		})
	}
}

func (prevFn SchemaᐸArgumentsᐳ) Fields(callback func(schema SchemaFieldsᐸArgumentsᐳ) SchemaFieldsᐸArgumentsᐳ) SchemaᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		return callback(func(struct_ Arguments) *validator.LoxeValidator {
			return v
		})(struct_)
	}
}

func (prevFn TestSchemaᐸArgumentsᐳ) Fields(callback func(TestSchemaFieldsᐸArgumentsᐳ) TestSchemaFieldsᐸArgumentsᐳ) TestSchemaᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		callback(func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {})(s, schema)
	}
}

func (prevFn SchemaᐸArgumentsᐳ) RootedFields(callback func(schema SchemaRootedFieldsᐸArgumentsᐳ) SchemaRootedFieldsᐸArgumentsᐳ) SchemaᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		return callback(func(struct_ Arguments) *validator.LoxeValidator {
			return v
		})(struct_)
	}
}

func (prevFn TestSchemaᐸArgumentsᐳ) RootedFields(callback func(TestSchemaRootedFieldsᐸArgumentsᐳ) TestSchemaRootedFieldsᐸArgumentsᐳ) TestSchemaᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		callback(func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {})(s, schema)
	}
}

func (prevFn SchemaFieldsᐸArgumentsᐳ) SourceFile(rule func(string) error) SchemaFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.SourceFile, rule(struct_.SourceFile))
		return v
	}
}

func (prevFn TestSchemaFieldsᐸArgumentsᐳ) SourceFile(test TestRuleᐸstringᐳ) TestSchemaFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value string) error {
			aux_variable := Arguments{}
			aux_variable.SourceFile = value
			return schema(aux_variable).Field(MetaᐸArgumentsᐳ.SourceFile)
		})
	}
}

func (prevFn SchemaRootedFieldsᐸArgumentsᐳ) SourceFile(rule func(Arguments) error) SchemaRootedFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.SourceFile, rule(struct_))
		return v
	}
}

func (prevFn TestSchemaRootedFieldsᐸArgumentsᐳ) SourceFile(test TestRuleᐸArgumentsᐳ) TestSchemaRootedFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value Arguments) error {
			return schema(value).Field(MetaᐸArgumentsᐳ.SourceFile)
		})
	}
}

func NewTestRuleᐸstringᐳ() TestRuleᐸstringᐳ {
	return func(s validator.TestSuite, rule func(string) error) {}
}

type TestRuleᐸstringᐳ func(s validator.TestSuite, rule func(string) error)

func (prevFn TestRuleᐸstringᐳ) Pass(value string) TestRuleᐸstringᐳ {
	return func(s validator.TestSuite, rule func(string) error) {
		prevFn(s, rule)

		e := rule(value)
		s.Helper()
		if e != nil {
			s.Fatalf("Value '%+v' should be considered valid\n\tError returned: %+v", value, e)
			return
		}
	}
}

func (prevFn TestRuleᐸstringᐳ) Fail(value string, expectedError validator.ComparableError) TestRuleᐸstringᐳ {
	if expectedError == nil {
		return prevFn
	}

	return func(s validator.TestSuite, rule func(string) error) {
		prevFn(s, rule)

		e := rule(value)
		s.Helper()
		if e == nil {
			s.Fatalf("Value '%+v' should be considered invalid\n\tError returned: nil", value)
			return
		}
		if !expectedError.Is(e) {
			s.Fatalf("Value '%+v' should be considered invalid\n\tError returned: %+v\n\tError expected: %+v", value, e, expectedError)
			return
		}
	}
}

func Intersectionᐸstringᐳ(rules ...func(string) error) func(string) error {
	return func(s string) error {
		for _, currRule := range rules {
			e := currRule(s)
			if e != nil {
				return e
			}
		}
		return nil
	}
}

func Unionᐸstringᐳ(rules ...func(string) error) func(string) error {
	return func(s string) error {
		var e error
		for _, currRule := range rules {
			e = currRule(s)
			if e == nil {
				return nil
			}
		}
		return e
	}
}

func Notᐸstringᐳ(rule func(string) error, newError error) func(string) error {
	return func(s string) error {
		e := rule(s)
		if e == nil {
			return newError
		}
		return nil
	}
}

func (prevFn SchemaFieldsᐸArgumentsᐳ) SourceType(rule func(string) error) SchemaFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.SourceType, rule(struct_.SourceType))
		return v
	}
}

func (prevFn TestSchemaFieldsᐸArgumentsᐳ) SourceType(test TestRuleᐸstringᐳ) TestSchemaFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value string) error {
			aux_variable := Arguments{}
			aux_variable.SourceType = value
			return schema(aux_variable).Field(MetaᐸArgumentsᐳ.SourceType)
		})
	}
}

func (prevFn SchemaRootedFieldsᐸArgumentsᐳ) SourceType(rule func(Arguments) error) SchemaRootedFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.SourceType, rule(struct_))
		return v
	}
}

func (prevFn TestSchemaRootedFieldsᐸArgumentsᐳ) SourceType(test TestRuleᐸArgumentsᐳ) TestSchemaRootedFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value Arguments) error {
			return schema(value).Field(MetaᐸArgumentsᐳ.SourceType)
		})
	}
}

func (prevFn SchemaFieldsᐸArgumentsᐳ) Debug(rule func(bool) error) SchemaFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.Debug, rule(struct_.Debug))
		return v
	}
}

func (prevFn TestSchemaFieldsᐸArgumentsᐳ) Debug(test TestRuleᐸboolᐳ) TestSchemaFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value bool) error {
			aux_variable := Arguments{}
			aux_variable.Debug = value
			return schema(aux_variable).Field(MetaᐸArgumentsᐳ.Debug)
		})
	}
}

func (prevFn SchemaRootedFieldsᐸArgumentsᐳ) Debug(rule func(Arguments) error) SchemaRootedFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.Debug, rule(struct_))
		return v
	}
}

func (prevFn TestSchemaRootedFieldsᐸArgumentsᐳ) Debug(test TestRuleᐸArgumentsᐳ) TestSchemaRootedFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value Arguments) error {
			return schema(value).Field(MetaᐸArgumentsᐳ.Debug)
		})
	}
}

func NewTestRuleᐸboolᐳ() TestRuleᐸboolᐳ {
	return func(s validator.TestSuite, rule func(bool) error) {}
}

type TestRuleᐸboolᐳ func(s validator.TestSuite, rule func(bool) error)

func (prevFn TestRuleᐸboolᐳ) Pass(value bool) TestRuleᐸboolᐳ {
	return func(s validator.TestSuite, rule func(bool) error) {
		prevFn(s, rule)

		e := rule(value)
		s.Helper()
		if e != nil {
			s.Fatalf("Value '%+v' should be considered valid\n\tError returned: %+v", value, e)
			return
		}
	}
}

func (prevFn TestRuleᐸboolᐳ) Fail(value bool, expectedError validator.ComparableError) TestRuleᐸboolᐳ {
	if expectedError == nil {
		return prevFn
	}

	return func(s validator.TestSuite, rule func(bool) error) {
		prevFn(s, rule)

		e := rule(value)
		s.Helper()
		if e == nil {
			s.Fatalf("Value '%+v' should be considered invalid\n\tError returned: nil", value)
			return
		}
		if !expectedError.Is(e) {
			s.Fatalf("Value '%+v' should be considered invalid\n\tError returned: %+v\n\tError expected: %+v", value, e, expectedError)
			return
		}
	}
}

func Intersectionᐸboolᐳ(rules ...func(bool) error) func(bool) error {
	return func(s bool) error {
		for _, currRule := range rules {
			e := currRule(s)
			if e != nil {
				return e
			}
		}
		return nil
	}
}

func Unionᐸboolᐳ(rules ...func(bool) error) func(bool) error {
	return func(s bool) error {
		var e error
		for _, currRule := range rules {
			e = currRule(s)
			if e == nil {
				return nil
			}
		}
		return e
	}
}

func Notᐸboolᐳ(rule func(bool) error, newError error) func(bool) error {
	return func(s bool) error {
		e := rule(s)
		if e == nil {
			return newError
		}
		return nil
	}
}

func (prevFn SchemaFieldsᐸArgumentsᐳ) SupportsANSI(rule func(bool) error) SchemaFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.SupportsANSI, rule(struct_.SupportsANSI))
		return v
	}
}

func (prevFn TestSchemaFieldsᐸArgumentsᐳ) SupportsANSI(test TestRuleᐸboolᐳ) TestSchemaFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value bool) error {
			aux_variable := Arguments{}
			aux_variable.SupportsANSI = value
			return schema(aux_variable).Field(MetaᐸArgumentsᐳ.SupportsANSI)
		})
	}
}

func (prevFn SchemaRootedFieldsᐸArgumentsᐳ) SupportsANSI(rule func(Arguments) error) SchemaRootedFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.SupportsANSI, rule(struct_))
		return v
	}
}

func (prevFn TestSchemaRootedFieldsᐸArgumentsᐳ) SupportsANSI(test TestRuleᐸArgumentsᐳ) TestSchemaRootedFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value Arguments) error {
			return schema(value).Field(MetaᐸArgumentsᐳ.SupportsANSI)
		})
	}
}

func (prevFn SchemaFieldsᐸArgumentsᐳ) OnlyAsciiTypenames(rule func(bool) error) SchemaFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.OnlyAsciiTypenames, rule(struct_.OnlyAsciiTypenames))
		return v
	}
}

func (prevFn TestSchemaFieldsᐸArgumentsᐳ) OnlyAsciiTypenames(test TestRuleᐸboolᐳ) TestSchemaFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value bool) error {
			aux_variable := Arguments{}
			aux_variable.OnlyAsciiTypenames = value
			return schema(aux_variable).Field(MetaᐸArgumentsᐳ.OnlyAsciiTypenames)
		})
	}
}

func (prevFn SchemaRootedFieldsᐸArgumentsᐳ) OnlyAsciiTypenames(rule func(Arguments) error) SchemaRootedFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.OnlyAsciiTypenames, rule(struct_))
		return v
	}
}

func (prevFn TestSchemaRootedFieldsᐸArgumentsᐳ) OnlyAsciiTypenames(test TestRuleᐸArgumentsᐳ) TestSchemaRootedFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value Arguments) error {
			return schema(value).Field(MetaᐸArgumentsᐳ.OnlyAsciiTypenames)
		})
	}
}

func NewTestRuleᐸArgumentsᐳ() TestRuleᐸArgumentsᐳ {
	return func(s validator.TestSuite, rule func(Arguments) error) {}
}

type TestRuleᐸArgumentsᐳ func(s validator.TestSuite, rule func(Arguments) error)

func (prevFn TestRuleᐸArgumentsᐳ) Pass(value Arguments) TestRuleᐸArgumentsᐳ {
	return func(s validator.TestSuite, rule func(Arguments) error) {
		prevFn(s, rule)

		e := rule(value)
		s.Helper()
		if e != nil {
			s.Fatalf("Value '%+v' should be considered valid\n\tError returned: %+v", value, e)
			return
		}
	}
}

func (prevFn TestRuleᐸArgumentsᐳ) Fail(value Arguments, expectedError validator.ComparableError) TestRuleᐸArgumentsᐳ {
	if expectedError == nil {
		return prevFn
	}

	return func(s validator.TestSuite, rule func(Arguments) error) {
		prevFn(s, rule)

		e := rule(value)
		s.Helper()
		if e == nil {
			s.Fatalf("Value '%+v' should be considered invalid\n\tError returned: nil", value)
			return
		}
		if !expectedError.Is(e) {
			s.Fatalf("Value '%+v' should be considered invalid\n\tError returned: %+v\n\tError expected: %+v", value, e, expectedError)
			return
		}
	}
}

func IntersectionᐸArgumentsᐳ(rules ...func(Arguments) error) func(Arguments) error {
	return func(s Arguments) error {
		for _, currRule := range rules {
			e := currRule(s)
			if e != nil {
				return e
			}
		}
		return nil
	}
}

func UnionᐸArgumentsᐳ(rules ...func(Arguments) error) func(Arguments) error {
	return func(s Arguments) error {
		var e error
		for _, currRule := range rules {
			e = currRule(s)
			if e == nil {
				return nil
			}
		}
		return e
	}
}

func NotᐸArgumentsᐳ(rule func(Arguments) error, newError error) func(Arguments) error {
	return func(s Arguments) error {
		e := rule(s)
		if e == nil {
			return newError
		}
		return nil
	}
}

var MetaᐸArgumentsᐳ = struct {
	SourceFile         string
	SourceType         string
	Debug              string
	SupportsANSI       string
	OnlyAsciiTypenames string
}{
	SourceFile:         "source-file",
	SourceType:         "source-type",
	Debug:              "debug",
	SupportsANSI:       "supports-ansi",
	OnlyAsciiTypenames: "only-ascii-typenames",
}

// Code generated by gitlab.com/loxe-tools/go-validation v1.0.41, DO NOT EDIT.
