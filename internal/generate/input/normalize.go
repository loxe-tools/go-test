package input

import (
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"path/filepath"
)

func Normalize(arguments Arguments, log *logCLI.LogCLI) Arguments {
	if !filepath.IsAbs(arguments.SourceFile) {
		abs, e := filepath.Abs(arguments.SourceFile)
		if e != nil {
			log.Fatal("%v", e)
		}
		arguments.SourceFile = abs
		log.Debug("--%s path converted to it's absolute path equivalent", MetaᐸArgumentsᐳ.SourceFile)

	}

	return arguments
}
