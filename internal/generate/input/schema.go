package input

var schemaArguments = NewSchemaᐸArgumentsᐳ().
	Fields(func(schema SchemaFieldsᐸArgumentsᐳ) SchemaFieldsᐸArgumentsᐳ {
		return schema.
			SourceFile(sourceFileRule).
			SourceType(sourceTypeRule)
	})
