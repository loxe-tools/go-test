package internal

import (
	"fmt"
	"runtime"
)

func FnCallFileLine(skip uint) string {
	_, file, line, ok := runtime.Caller(int(skip + 1))
	if !ok {
		return "## Cannot recover the filepath and line number of this function call ##"
	}

	return fmt.Sprintf("%s:%d", file, line)
}
